"use strict";

const Controller = require("egg").Controller;
const AlipaySdk = require("alipay-sdk");
const AlipayFormData = require("alipay-sdk/lib/form");
// 普通公钥模式
const alipaySdk = new AlipaySdk({
  // 参考下方 SDK 配置
  appId: "2021000118615308",
  privateKey:"MIIEogIBAAKCAQEAkrtwETwiom2853F4u0yVwKkjuXO/bgXapGHa3CwFEGJZiCHS2GGbl7goiwR3uZnuTtKDjLTjgcH1vLBqTLyop2KGszUvZ4+lftzRArOWM4FxL9/s/yiEyjnfhXjzjqb+cxrHwnfE2uDaofN+tVpsBH/uFScgwbzoUE/qx+dT9SkkTRfkMDiVSAqLLr3XCoDUq06JNGLfd1vMyIA42X27WSdT+K5T0U59cpvhXwNB88rNiVaJaQK1j9Zof5JAuc6+Nv8pbuGzFlSOY7P6ztmYtA/SEL37LPfef0Tz7Gq4ReYVZkDte95f+o7g9QqUKwyvsXMvkqukD617e1GH33tvvwIDAQABAoIBAAMJNcxlfiH2O05enqzhbMfLuB2CM5fHLzrve2PK1V/9aPTUHlSz0FBXUx9tPDzu3mtrF6W1qG9q3/bDf6+Ox76O31Kd6xktc7MoRnrT1GvEZd8jtvegkv7MK7/LPN7avbEMAM4ZRE1zEZMAQsLl2A5Px1FQufDIQ9ZFkQoJ00bjRtl+LuAQKKxcbRdeMRWMxGgKbgZp3rufZpeduhx2WYrT/v/w5mgayTEZ+RCs4vSpk7gi+d9wtzJJZQ+IbFWNX6xo0PZvdB+RG4qFurg3brq4r7u2x7gxlQUZYsJ3XRUiFFhWl8oOtwAGUappH1R3odEnSqxRLdDj0E9gbeN/S0ECgYEA7VqTfE3MiTXIfWDFnoBYC/gN1jkXZXXfX+SJXdixZ8XPZLUirIibhbj0NP7l0UaFYPxBT5Cc8OJx64GiUalDouC8pDi+OkYK0KdW8W5WtYjvIg3PJGqfXUFv1pHb3okDkVl1TVymfDuWjJ7z62x70kZX8tnEN2uoL9ibt24ZuqcCgYEAnkJelAPU4Cx7LAzIj26fuvRqmSWajVuz46L1xjNOl1FanDU3YyE0nF8xdYD5QxMeFWJI8+oIYrHP2DECFTR3/w8uoIniC4ACK03esKfYa3I5JPxqyt7L0MhUAjIiMT4s/3CtZRi0Sq+eJWDjvBQYXOR6a52+/4G7ECFwGbUffSkCgYBxhmsYf1WfIxxb65dZ8mUqhGCJ4BCytKg8g2C4hFaGHfTOZFzLTSOl2Arl9i0R5bsR4lWSqsaF6Kk4TW7HxD/Xfcp8ea9yy+GMXx2UqIPMnExUa2pyLpU+03ecl3Mkg0Azmygh8nim3xFo/PYJ2n922bJjM4lIQLPW/rGsEHs57QKBgHeLiOfkEQyZbR10NpfqJ2xQrqkqt5lqGPOuKFtkbExvXrO6mzEdZyFr4u9w0lkxersKeDMPqO/sgyU6t+kOrkoTzNPl88NeGcVeWAY3c4qOSM5Agebm7yyx9ouGFT//SHxYQBKq253DFQIFM2HiCBk7WaIrEZjuBUEFESnKIJQpAoGACzv8ezTjIjrhyAph4Ez0h4MVTVmW4xVEIJJUPDfmE5HmSia3z6275TEavXVYaMwgMIlV2oHUHVn68ms4XXisYc9Iucl8bh87C9q5fP2sW76Sr8szWetMtUseUTJuibeutfG0BkBc/kYGzAc0YfrKgNLpIGlPzGX90C6FTvtUIDo=",
  //可设置AES密钥，调用AES加解密相关接口时需要（可选）
  encryptKey: "RUH15CmDIWMG1OmO7ImGtg==",
});
class PayController extends Controller {
  async create() {
    const { ctx } = this;
    const { totalAmount, id } = this.ctx.request.body;
    const formData = new AlipayFormData();
    // 调用 setMethod 并传入 get，会返回可以跳转到支付页面的 url
    formData.setMethod("get");
    formData.addField("notifyUrl", "http://www.com/notify");
    formData.addField("bizContent", {
      outTradeNo: '1234',
      productCode: "FAST_INSTANT_TRADE_PAY",
      totalAmount:'1000',
      subject: "商品",
      body: "商品详情",
    });

    const result = await alipaySdk.exec(
      "alipay.trade.page.pay",
      {},
      { formData: formData }
    );

    // result 为可以跳转到支付链接的 url
    this.ctx.body = {
      data: {
        url: result,
      },
    };
    console.log(result);
  }
}

module.exports = PayController;
