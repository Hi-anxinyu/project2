"use strict";
const Controller = require("egg").Controller;
//引入 nodemailer
const nodemailer = require("nodemailer");
class MailController extends Controller {
  async echo() {}
  async getTransorter() {
    // 使用默认SMTP传输创建可重用传输器对象  
    let transporter = nodemailer.createTransport({
      host: "smtp.qq.com",
      port: 465,
      secure: true, // 对于465端口为True，对于其他端口为true
      auth: {
        user: "1778120064", // 生成的用户
        pass: "kzdqgfsnupacdgbg", // 生成的密码  生成的密码必须是发送邮件的邮箱所生成的
      },
    });
    return transporter;
  }
  async create() {
    let { ctx } = this;
    let { to, from, subject, html } = ctx.request.body;
    try {
      let transporter = await this.getTransorter();
      let info = await transporter.sendMail({
        from: "👻自动发件👻<1778120064@qq.com>", // 发送邮件的邮箱
        to: to || "1778120064@qq.com,1509914000@qq.com", // 接收邮件的列表
        subject: subject || "Hello -爱你么么哒✔", // 主题
        html: html || "<div><h1>爱你么么哒</h1></div>", // html结构
      });
      this.ctx.body = {
        data: {
          preview: "发送邮件成功",
        },
      };
    } catch (error) {
      this.ctx.body = {
        data: {
          message: "邮件发送失败",
          error,
        },
      };
    }
  }
}
module.exports = MailController;
