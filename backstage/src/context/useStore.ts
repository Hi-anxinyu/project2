import { useContext } from "react";
import stateContext from "./stateContext";

export default function () {
    return useContext(stateContext)
}