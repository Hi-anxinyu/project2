import User from './module/user';
import Article from './module/article';
import Comment from './module/comment';
import Knowledge from "./module/knowledge";
import Search from './module/search';
import Mail from './module/mail';
import Users from './module/users';
import File from './module/file'
import Tag from './module/tag';
import Editor from './module/editor';
import Category from './module/category';
import Page from './module/page';
import Setting from './module/setting'
import Tags from './module/tags'
export default{
    user: new User,
    article:new Article,
    comment:new Comment,
    knowledge: new Knowledge,
    mail:new Mail,
    users:new Users,
    search:new Search,
    tag:new Tag,
    file:new File,
    editor: new Editor,
    Category: new Category,
    page:new Page,
    setting:new Setting,
    Tags: new Tags,
}
