import { getTagList, addTagsList, removeTagsList } from '@/service';
import { IEditorList } from '@/types/edioor';
import { makeAutoObservable, runInAction } from 'mobx';
import { message } from 'antd'
class Tags {
  tagsList: IEditorList[] = [];
  constructor() {
    makeAutoObservable(this)
  }
  async getTagsList() {
    let result = await getTagList();
    runInAction(() => {
      this.tagsList = result.data;
    })
  }
  async addTagsList(data: {label: string, value: string}) {
    let result = await addTagsList(data);
    if (result) {
      message.success('操作成功');
    }
  }
  async removeTagsList(id: string) {
    let result = await removeTagsList(id);
    if (result) {
      message.success('操作成功');
    }
  }
}

export default Tags;