import { makeAutoObservable, runInAction } from 'mobx';
import {
  getUserList, getUserUpdate,
} from '@/service/index';
import { message } from 'antd';
import { IUserItem } from '@/types';
import { IPayload } from '@/types';

class Users{
    userList:IUserItem[] = [] 
    total:number = 0
    constructor(){
        makeAutoObservable(this)
    }
    async getUserList(data: IPayload) {
        let result = await getUserList(data);
        if (result) {
          runInAction(() => {
            this.userList=result.data[0];
            this.total=result.data[1]
          });
        }
      }
    async getUserUpdate(data:IUserItem){
      let result = await getUserUpdate(data);
      if(result.statusCode === 201) {

      }else{
        message.error('报错了')
      }
    }
}

export default Users