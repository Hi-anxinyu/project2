//mobx仓库
import { addKnowledgeList, changeKnowledgeList, deleteKnowledgeList, getFileList, getKnowledgeList, setKnowledgeList } from "@/service";
import { IAddKnowlwdge, IFileList, IKnowledge } from "@/types";
import { makeAutoObservable } from "mobx"
class Knowledge {
    knowledgeList: IKnowledge[] = [];//数据
    knowledgeCount = 0;//数据的数量
    editorList = [];
    addKnowledgeList: Partial<IAddKnowlwdge> = {};//添加数据的对象
    fileList: IFileList[] = [];//文件的数据
    fileCount = 0;//文件数据的对象
    constructor() {
        makeAutoObservable(this);
    }
    //获取数据
    async getKonwledgeList(page: number, pageSize: number, params = {}) {
        let result = await getKnowledgeList(page, pageSize, params);
        if (result.statusCode === 200) {
            this.knowledgeList = result.data[0];
            this.knowledgeCount = result.data[1];
        }
    }
    //删除数据
    async deleteKonwledgeList(id: string) {
        let result = await deleteKnowledgeList(id);
        console.log(result);
    }
    //添加数据
    async addKonwledgeList({ title, summary, isCommentable, cover }: Partial<IAddKnowlwdge> = {}) {
        let result = await addKnowledgeList({ title, summary, isCommentable, cover });
        console.log(result);
    }
    //改变发布草稿
    async changeKonwledgeList(id: string, status: string) {
        let result = await changeKnowledgeList(id, status);
        console.log(result);
    }
    //获取文件数据
    async getFileList(page: number, pageSize: number, params = {}) {
        let result = await getFileList(page, pageSize, params);
        console.log(result);
        if (result.statusCode === 200) {
            this.fileList = result.data[0];
            this.fileCount = result.data[1];
        }
    }
    //改变数据
    async setKnowledgeList(id: string, { title, summary, isCommentable, cover }: Partial<IAddKnowlwdge> = {}) {
        let result = await setKnowledgeList(id, { title, summary, isCommentable, cover });
        console.log(result);
    }
}
export default Knowledge