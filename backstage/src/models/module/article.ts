import { makeAutoObservable, runInAction } from 'mobx';
import { getArticleList, getCategoryList, changeArticleStatus, changeArticleRecommended, deleteRaticle } from '@/service';
import { IArticleList, ICategoryList } from '@/types';
import { message } from 'antd'
class Article {
  articleList: IArticleList[] = [];
  total: number = 0;
  categoryList: ICategoryList[] = [];
  defaultArticle: string = ''
  constructor() {
    makeAutoObservable(this);
  }
  async getArticleList(data: { page?: number; pageSize?: number; title?: string; status?: string, category?: string }) {
    let result = await getArticleList(data);
    if (result) {
      runInAction(() => {
        this.articleList = result.data[0];
        this.total = result.data[1];
        this.defaultArticle = result.data[0][0].content;
      })
    }
  }
  async getCategoryList() {
    let result = await getCategoryList();
    if (result) {
      runInAction(() => {
        this.categoryList = result.data;
      })
    }
  }
  async changeArticleStatus(data: { id: string[], status: string }) {
    Promise.all(data.id.map(item => changeArticleStatus({ id: item, status: data.status }))).then((res) => {
      message.success('操作成功')
      return res;
    }).catch( error => {
      message.error('操作失败')
    })
  }
  async changeArticleRecommended(data: { id: string[], isRecommended: boolean }) {
    Promise.all(data.id.map(item => changeArticleRecommended({ id: item, isRecommended: data.isRecommended }))).then((res) => {
      message.success('操作成功')
    }).catch(error => {
      message.error('操作失败')
    })
  }
  async deleteRaticle(id: string[]) {
    Promise.all(id.map(item => deleteRaticle(item))).then((res) => {
      message.success('操作成功')
    }).catch(error => {
      message.error('操作失败')
    })
  }
}

export default Article;
