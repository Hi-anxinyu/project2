import { makeAutoObservable, runInAction } from 'mobx';
import {
  getCommentList,
  setAction,
  setDelete,
  setReply,
} from '@/service/index';
import { message } from 'antd';
import { ICommentItem } from '@/types';
import { Payload, IAction, IPayloadTo } from '../../types/comment';

class Comment {
  commentList: ICommentItem[] = [];
  total: number = 0;
  constructor() {
    makeAutoObservable(this);
  }
  //获取数据
  async getCommentList(data: Payload) {
    let result = await getCommentList(data);
    if (result) {
      runInAction(() => {
        this.commentList = result.data[0];
        this.total = result.data[1];
      });
    }
  }
  //全选通过
  //通过和拒绝
  async setAction(data: IAction) {
    let result = await setAction(data);
    if (result.statusCode === 200) {
    } else if(result.statusCode === 401){
      message.warning('用户权限错误')
    }
  }
  //删除
  async setDelete(id: string) {
    let result = await setDelete(id);
    if (result.statusCode === 200) {
    }else if(result.statusCode === 401){
      message.warning('用户权限错误')
    }
     else {
      message.error('错误');
    }
  }
  async setReply(data:IPayloadTo){
    let result = await setReply(data);
    if(result.statusCode ===201){
      message.success('回复成功')
    }else if(result.statusCode === 401){
      message.warning('用户权限错误')
    }
    else{
      message.error('回复失败')
    }
  }
}
export default Comment;
