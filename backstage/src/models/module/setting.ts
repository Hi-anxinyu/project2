import { setSave, getSettingDate } from "@/service";
import { makeAutoObservable, runInAction } from "mobx";
import {ISettingItem,IPayloadts} from '@/types'
import {message} from 'antd'
class Setting {
    settingItem:ISettingItem={} as ISettingItem
    constructor() {
        makeAutoObservable(this);
    }
    async getSettingDate(){
        let result = await getSettingDate()
        if(result.statusCode===200){
            this.settingItem=result.data
        }
    }
    async setSave(data:IPayloadts){
        let result = await setSave(data)
        if(result.statusCode===201){
           message.success('保存成功')
        }else{
           message.error('保存失败')
        }
    }

}
export default Setting