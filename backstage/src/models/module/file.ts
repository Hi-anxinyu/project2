import { makeAutoObservable, runInAction } from 'mobx';
import {IPayload,IFileItem} from '@/types'
import { getFileLists, setDelete, setDeletes } from '@/service';
import {message} from 'antd'
class File {
    fileList:IFileItem[]=[];
    total:number = 0;
  constructor() {
    makeAutoObservable(this);
  }
  async getFileList(data:IPayload){
      let result = await getFileLists(data);
        if(result.statusCode === 200){
            this.fileList=result.data[0]
            this.total=result.data[1]
        } 
  }
  async setDelete(id:string){
    let result = await setDeletes(id)
    if(result.statusCode === 200){
        message.success('操作成功')
    }else{
        message.error('操作失败')
    }
  }
}

export default File;
