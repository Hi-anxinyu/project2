import { getCategoryList, addCategoryList, deleteCategoryList, updateCategory } from '@/service';
import { ICategoryList } from '@/types';
import { makeAutoObservable, runInAction } from 'mobx'
import { message } from 'antd'
class Category {
  categoryList: ICategoryList[] = [];
  constructor() {
    makeAutoObservable(this);
  }
  async getCategoryList() { // 获取分类列表数据
    let result = await getCategoryList();
    if (result) {
      runInAction(() => {
        this.categoryList = result.data;
      })
    }
  }
  async addCategoryList(data: {label: string, value: string}) { // 添加分类列表数据
    let result = await addCategoryList(data);
    if (result) {
      runInAction(() => {
        this.categoryList = result.data;
      })
    }
  }
  async deleteCategoryList(id: string) { // 添加分类列表数据
    let result = await deleteCategoryList(id);
    if (result.data) {
      message.success('删除成功')
    }
  }
  async updateCategory(data: {label: string, value: string, id: string}) { // 更新分类列表数据
    let result = await updateCategory(data);
    if (result) {
      message.success('更新分类成功')
    }
  }
}
export default Category;