import { ITagItem } from '@/types';
import { makeAutoObservable, runInAction } from 'mobx';
import { getTagList } from '@/service';
class Tag {
  taglist: ITagItem[] = [];
  tagtotal: number = 0;
  constructor() {
    makeAutoObservable(this);
  }
  getTagList = async () => {
    let res = await getTagList();
    if (res.statusCode === 200) {
      runInAction(() => {
        this.taglist = res.data;
        this.tagtotal = res.data.length;
      });
    }
  };
}

export default Tag;
