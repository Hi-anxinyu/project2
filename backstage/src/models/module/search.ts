import { deleteSearchList, getSearchList } from "@/service";
import { makeAutoObservable } from "mobx";
import { ISearch, ISearchList } from '@/types/index';
class Search {
    searchList: ISearch[] = [];
    searchCount = 0;
    constructor() {
        makeAutoObservable(this);
    }
    async getSearchList(page: number, pageSize: number, params: Partial<ISearchList> = {}) {
        let result = await getSearchList(page, pageSize, params);
        // console.log(result);
        if (result.statusCode === 200) {
            this.searchList = result.data[0];
            this.searchCount = result.data[1];
            // console.log(this.searchList);
        }
    }
    async deleteSearchList(id: string[]) {
        let result = await deleteSearchList(id);
        // console.log(result);
    }
}
export default Search