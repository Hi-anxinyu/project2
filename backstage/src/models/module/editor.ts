import { getEditorDetail, getTagList, gitFileList } from '@/service/module/editor'
import { IArticleList } from '@/types';
import { IEditorList } from '@/types/edioor';
import { makeAutoObservable, runInAction } from 'mobx'

class Editor {
  tagList: IEditorList[] = []
  editorDetail: IArticleList = ({} as IArticleList);
  constructor() {
    makeAutoObservable(this)
  }
  async getTagList() {
    let result = await getTagList();
    if (result.statusCode === 200) {
      runInAction(() => {
        this.tagList = result.data;
      })
    }
  }
  async getFileList(data: { page: number, pageSize: number }) {
    let result = await gitFileList(data);
  }
  async getEditorDetail(id: string) {
    let result = await getEditorDetail(id);
    if (result) {
      runInAction(() => {
        this.editorDetail = result.data;
      })
    }
  }
}
export default Editor;