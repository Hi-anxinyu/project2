import {makeAutoObservable,runInAction} from 'mobx';
import {IPageItem} from '@/types/page';
import {getPageList,IPageParams,publishPage} from '@/service';
class Page{
    pagelist:IPageItem[] = [];
    pagetoal:number = 0;
    constructor(){
        makeAutoObservable(this);
    }
    async getPageList(params:Partial<IPageParams>){
        let res = await getPageList(params);
        if(res.statusCode === 200){
            runInAction(()=>{
                this.pagelist = res.data[0];
                this.pagetoal = res.data[1];
            })
        }
    }
    async publishPage(arr:string[],type:string){
        let result = null;
        for(let i=0;i<arr.length;i++){
            result =   await publishPage({id:arr[i],type})
        }
    }
}

export default Page;