import { makeAutoObservable, runInAction } from 'mobx';
import { getSmtpList, deleteMail } from '@/service/index';
import { IMailItem, ISmtpItem } from '@/types';
import { message } from 'antd';
class Mail {
  mailList: IMailItem[] = [];
  total: number = 0;
  constructor() {
    makeAutoObservable(this);
  }
  async getSmtpList(params:Partial<ISmtpItem>) {
    let result = await getSmtpList(params);
    if (result) {
      runInAction(() => {
        this.mailList = result.data[0];
        this.total = result.data[1];
      });
    }
  }
  async deleteMail(ids: Array<string>) {
    message.loading('删除中');
    Promise.all(ids.map((id) => deleteMail(id)))
      .then(() => {
        message.destroy();
        message.success('删除成功');
        this.getSmtpList({});
      })
      .catch(() => {
        message.success('删除成功');
        this.getSmtpList({});
      });
  }
}

export default Mail;
