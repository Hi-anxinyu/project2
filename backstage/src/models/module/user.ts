import { getLoginData } from '@/service';
import { ILoginForm, IUserInfo } from '@/types';
import { removeToken, setToken } from '@/utils';
import { makeAutoObservable, runInAction } from 'mobx';
import { message } from 'antd';
class User {
  isLogin = false;
  userInfo: Partial<IUserInfo> = {};
  constructor() {
    makeAutoObservable(this);
  }
  async login(data: ILoginForm) {
    let result = await getLoginData(data);
    if (result.data) {
      message.success('登陆成功');
      runInAction(() => {
        this.isLogin = true;
        this.userInfo = result.data;
      });
      setToken(result.data.token);
    } 
    return result.data;
  }
  //退出登录
  logout() {
    removeToken();
  }
}

export default User;
