import React, { useEffect, useState } from 'react'
import styles from './comment.less'
import { Space, Popconfirm, message, Popover, Input, Breadcrumb } from 'antd';
import From from '../../components/comment/From';
import moment from 'moment';
import { ICommentItem } from '@/types/index';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { Payload, Values } from '@/types/comment'
import Tables from '@/components/comment/Table'
import Footer from '@/components/Footer/Footer';
import PopUp from '@/components/comment/PopUp';
import BreadHeader from '@/components/Header/BreadHeader';
import { useHistory } from 'umi';
const Comment: React.FC = () => {
  //获取数据
  const { TextArea } = Input;
  const store = useStore()
  const history = useHistory();
  const pathname = history.location.pathname;
  const [titleArr, settitleArr] = useState(['评论管理'])
  const [page, setPage] = useState(1)
  const [pageSize, setPageSize] = useState(['8', '12', '24', '36'])
  //获取数据
  const { commentList, total } = store.comment;
  //设置Table表格load动画
  const [flag, setFlag] = useState(false)
  const [arrId, setArrId] = useState([])

  //保存id
  const [item,setItem] = useState({} as ICommentItem)
  //封装初始化
  function initData(data: Payload) {
    setFlag(true)
    store.comment.getCommentList(data).then(() => {
      setFlag(flag)
    })
  }
  //请求
  useEffect(() => {
    initData({ page })
  }, [])
  //搜索框From表单
  const onFinish = (values: Values) => {
    console.log(values);
    initData({ page, name: values.name, pass: values.pass, email: values.email })
  };

  //表格
  const columns = [
    {
      title: '状态',
      width: 100,
      dataIndex: 'pass',
      key: 'id',
      fixed: 'left',
      render: (value: boolean, row: ICommentItem, index: number) => {

        return (
          <div className={styles.comment_flag}>
            <span className={commentList[index].pass ? styles.green : styles.orange}>
            </span>
            <span>{commentList[index].pass ? '通过' : '未通过'}
            </span>
          </div>
        )
      }
    },
    {
      title: '称呼',
      width: 100,
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '联系方式',
      dataIndex: 'email',
      key: '1',
      width: 160,
    },
    {
      title: '原始内容',
      dataIndex: 'address',
      key: '2',
      width: 150,
      fixed: 'top',
      render: (value: boolean, row: ICommentItem, index: number) => {
        const content = (

          <div className={styles.comment_Mask_disp}>
            <p dangerouslySetInnerHTML={{ __html: row.html }}></p>
          </div>

        );
        return (
          <div className={styles.comment_Mask}>
            <Popover
              placement="top"
              content={content}
              title="评论详情-原始内容"
              trigger="hover"
              autoAdjustOverflow={true}
            >
              <a href="#">查看内容</a>
            </Popover>

          </div>
        )
      }
    },
    {
      title: 'HTML内容',
      dataIndex: 'address',
      key: '3',
      width: 150,
      render: (value: boolean, row: ICommentItem, index: number) => {
        const content = (

          <div className={styles.comment_Mask_disp}>
            <p dangerouslySetInnerHTML={{ __html: row.html }}></p>
          </div>

        );


        return (
          <div className={styles.comment_Mask}>
            <Popover
              placement="top"
              content={content}
              title="评论详情-HTML内容"
              trigger="hover"
              autoAdjustOverflow={true}
            >
              <a href="#">查看内容</a>
            </Popover>

          </div>
        )
      }
    },
    {
      title: '管理文章',
      dataIndex: 'address',
      key: '4',
      width: 150,
      render: (value: boolean, row: ICommentItem, index: number) => {
        const content = (
          <div className={styles.comment_article}>
              <iframe src={`https://creation.shbwyz.com/article/${row.hostId}`}></iframe>
          </div>
        );
        return (
          <div className={styles.comment_Mask}>
            <Popover
              title="页面预览"
              placement="left"
              content={content}
              trigger="hover"
              autoAdjustOverflow={true}
              getPopupContainer={(trigger: any) => trigger.parentElement}
              overlayClassName={styles.comment_mask_over}
              overlayStyle={{width:'350px',height:'200px',position:'fixed',marginTop:'-200px'}}
              overlayInnerStyle={{marginTop:'-200px',height:'550px'}}
              >
            <a href="#">文章</a>
          </Popover>
             
          </div>  
        )
      }
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
      key: '5',
      width: 150,
      render: (value: boolean, row: ICommentItem, index: number) => {
        return (
          <div>
            {moment(row.createAt).format("YYYY-MM-DD HH:mm:ss")}
          </div>
        )
      }
    },
    {
      title: '父级评论',
      dataIndex: '',
      key: '6',
      width: 150,
      render:(value: boolean, row: ICommentItem, index:number)=>{
        return(
          <div>{row.replyUserName===null?'无':row.replyUserName}</div>
        )
      }
    },
    {
      title: '操作',
      key: 'operation',
      fixed: 'right',
      textAlign: 'center',
      flex: 1,
      width: 200,
      render: (value: boolean, row: ICommentItem, index: number) =>
        <Space size="middle" className={'comment_table_edit'}>
          <a onClick={async () => {
           await store.comment.setAction({ id: row.id, pass: true })
           initData({ page })
            message.success('评论已通过')
          }}>通过</a>
          <a onClick={async () => {
            await store.comment.setAction({ id: row.id, pass: false })
            initData({ page })
            message.success('评论已拒绝')
          }}>拒绝</a>
          <a onClick={()=>showModal(row) as any}>回复</a>
          <Popconfirm
            title="确认删除?"
            onConfirm={async () => {
              await store.comment.setDelete(row.id)
              initData({ page })
              message.success('评论删除成功')
            }}
            okText="确认"
            cancelText="取消"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>,
    },
  ];
  //回复的弹出框
  const [visible, setVisible] = useState(false)
  const showModal = (row:any) => {
    setVisible(true);
    setItem(row)
  };
  //成功时
  const hideModal = () => {
    setVisible(false);
    
  };
  const [val, setVal] = useState<string>('')
  const ChangeVal =async ({ target: { value } }: any) => {
   await setVal(value)
  };
  //点击回复框确认按钮
  const clickAffirm=async ()=>{
    const user = JSON.parse(localStorage.getItem('user')!) || {}

  
     await store.comment.setReply(
      {
        content: val,
        createByAdmin: true,
        name: JSON.parse(localStorage.getItem('user')!).name || '',
        hostId:item.hostId,
        email: item.email ,
        parentCommentId: item.parentCommentId!,
        replyUserEmail: item.replyUserEmail!,
        replyUserName:item.replyUserName!,
        url:item.url
      })    
      console.log(item);
      
      setVal('')
      setTimeout(()=>{
        initData({page})
      },100)
      hideModal()
  } 
  
  //监听分页
  const onShowSizeChange = async (current: number, size: number) => {
    await setPage(current)
    initData({ page: current })
  }
  return (
    <div className={styles.comment}>
      <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
      <div className={styles.comment_search}>
        <From onFinish={onFinish} />
      </div>
      <div className={styles.comment_table}>
        <Tables
          columns={columns}
          page={page}
          pageSize={pageSize}
          initData={initData}
          flag={flag}
          commentList={commentList as unknown as ICommentItem}
          total={total}
          onShowSizeChange={onShowSizeChange}
        />
        <PopUp visible={visible}
          showModal={showModal}
          val={val}
          hideModal={hideModal}
          ChangeVal={ChangeVal} 
          item={item}
          clickAffirm={clickAffirm}
          />
      </div>
      <Footer></Footer>
    </div>
  )
}
export default observer(Comment)