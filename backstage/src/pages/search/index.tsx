import React, { Key } from "react"
import { Input, Button, Table, Form, Popconfirm, Row, Col, message, Popover, Tooltip } from 'antd';
import { ColumnProps } from 'antd/es/table';
import { useState } from "react";
import useStore from "@/context/useStore";
import moment from "moment"
import { useEffect } from "react";
import { observer } from "mobx-react-lite"
import { ISearch } from "@/types";
import Header from "@/components/search/Header";
import Footer from "@/components/Footer/Footer";
import "./index.less"

// 搜索记录页面
const Search: React.FC = () => {
    const store = useStore();
    const [form] = Form.useForm();
    const [page, setPage] = useState(1)
    const [pageSize, setPageSize] = useState(12)
    const [params, setParams] = useState({})
    const [selectedRowKeys, SetSelectedRowKeys] = useState([])
    const [id, setId] = useState("");
    const columns: ColumnProps<any>[] = [
        {
            title: '搜索词',
            dataIndex: 'keyword',
        },
        {
            title: '搜索量',
            dataIndex: 'count',
            render: (count) => {
                return <p style={{ width: "20px", height: "20px", background: "rgb(82, 196, 26)", color: "#fff", borderRadius: "50%", textAlign: "center", lineHeight: "20px" }}>{count}</p>
            }
        }, {
            title: '搜索时间',
            dataIndex: 'createAt',
            render: (createAt) => {
                return <span>{moment(createAt).format("YYYY-MM-DD HH:mm:ss")}</span>
            }
        }, {
            title: '操作',
            render: (record: ISearch) => {
                return <Popconfirm title="确定要删除吗" okText="确认" cancelText="取消" onConfirm={async () => {
                    await store.search.deleteSearchList([record.id]);
                    await store.search.getSearchList(page, pageSize, params);
                }}><a>删除</a></Popconfirm>
            }
        },
    ]
    // 点击全选框触发
    const onSelectChange = (selectedRowKeys: any, selectedRows: any) => {
        SetSelectedRowKeys(selectedRowKeys)
    }
    // 多选
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    // 点击搜索
    function submit() {
        let values = form.getFieldsValue();
        console.log(values, "搜索页面值");
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
    }
    //刷新
    function refresh() {
        store.search.getSearchList(page, pageSize, params);
    }
    async function confirm() {
        await store.search.deleteSearchList(selectedRowKeys as string[]);
        await store.search.getSearchList(page, pageSize, params);
        message.success('Click on Yes');
    }
    function cancel() {
        message.error('Click on No');
    }
    useEffect(() => {
        store.search.getSearchList(page, pageSize, params);
    }, [page, pageSize, params])
    return (
        <div style={{
            overflow: "auto",
            height: "100%",
            width: "100%"
        }}>
            <Header />
            <main
                style={{
                    padding: "24px",
                    flex: "1",
                    overflow: "auto"
                }}
            >
                <div>
                    <div>
                        <Form
                            form={form}
                            name="advanced_search"
                            className="ant-advanced-search-form"
                            onFinish={submit}
                        >
                            <div style={{ display: "flex" }}>
                                <Row gutter={24}>
                                    <Col span={8}>
                                        <Form.Item
                                            name="type"
                                            label={"类型"}
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Input something!',
                                                },
                                            ]}
                                        >
                                            <Input placeholder="请输入搜索类型" style={{ width: 230 }} />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col span={8}>
                                        <Form.Item
                                            name="keyword"
                                            label={"搜索词"}
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Input something!',
                                                },
                                            ]}
                                        >
                                            <Input placeholder="请输入搜索词" style={{ width: 230 }} />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={24}>
                                    <Col span={8}>
                                        <Form.Item
                                            name="count"
                                            label={"搜索量"}
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Input something!',
                                                },
                                            ]}
                                        >
                                            <Input placeholder="请输入搜索量" style={{ width: 230 }} />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </div>
                            <Row>
                                <Col span={24} style={{ textAlign: 'right' }}>
                                    <Button type="primary" htmlType="submit">
                                        搜索
                                    </Button>
                                    <Button
                                        style={{ margin: '0 8px' }}
                                        onClick={() => {
                                            form.resetFields();
                                        }}
                                    >
                                        重置
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                        <div className="aaa">
                            <div className="_4yxeklcCZVaddgSjYswZG">
                                {
                                    selectedRowKeys.length > 0 && <Popconfirm
                                        title="确认删除?"
                                        onConfirm={confirm}
                                        onCancel={cancel}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <Button danger >
                                            删除
                                        </Button>
                                    </Popconfirm>

                                }
                                <div></div>
                                <div>
                                    <span onClick={refresh}>
                                        <Tooltip title="刷新">
                                            <svg viewBox="64 64 896 896" focusable="false" data-icon="reload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M909.1 209.3l-56.4 44.1C775.8 155.1 656.2 92 521.9 92 290 92 102.3 279.5 102 511.5 101.7 743.7 289.8 932 521.9 932c181.3 0 335.8-115 394.6-276.1 1.5-4.2-.7-8.9-4.9-10.3l-56.7-19.5a8 8 0 00-10.1 4.8c-1.8 5-3.8 10-5.9 14.9-17.3 41-42.1 77.8-73.7 109.4A344.77 344.77 0 01655.9 829c-42.3 17.9-87.4 27-133.8 27-46.5 0-91.5-9.1-133.8-27A341.5 341.5 0 01279 755.2a342.16 342.16 0 01-73.7-109.4c-17.9-42.4-27-87.4-27-133.9s9.1-91.5 27-133.9c17.3-41 42.1-77.8 73.7-109.4 31.6-31.6 68.4-56.4 109.3-73.8 42.3-17.9 87.4-27 133.8-27 46.5 0 91.5 9.1 133.8 27a341.5 341.5 0 01109.3 73.8c9.9 9.9 19.2 20.4 27.8 31.4l-60.2 47a8 8 0 003 14.1l175.6 43c5 1.2 9.9-2.6 9.9-7.7l.8-180.9c-.1-6.6-7.8-10.3-13-6.2z"></path></svg>
                                        </Tooltip>
                                    </span>
                                </div>
                            </div>
                            <Table
                                rowSelection={rowSelection}
                                rowKey={"id"}
                                columns={columns}
                                dataSource={store.search.searchList}
                                pagination={{
                                    showSizeChanger: true,
                                }}
                            />
                        </div>
                    </div>
                </div>
                <Footer />
            </main>
        </div>
    )
}
export default observer(Search)