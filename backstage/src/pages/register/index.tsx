import styles from '../login/index.less';
import { Row, Col, Form, Input, Button, message, Modal, Space } from 'antd';
import { observer } from 'mobx-react-lite';
import { NavLink } from 'react-router-dom';
import { register } from '@/service'
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { useState} from 'react';
import {useHistory} from 'umi';
const registry = () => {
    const { confirm } = Modal;
    const history = useHistory();
    const onFinish = async (values: { name: string, password: string, confirm: string }) => {
        let res = await register(values);
        console.log(res);
        if (res.statusCode === 201) {
            showConfirm()
        } else if(res.statusCode === 400) {
            message.error(res.msg)
        }
    };
    function showConfirm() {
        confirm({
          title: '注册成功',
          icon: <ExclamationCircleOutlined />,
          content: '是否跳转登陆页面',
          onOk() {
            history.replace('/login');
          },
          onCancel() {
            console.log('Cancel');
          },
        });
      }
    return (
        <div className={styles.login_box}>
             <Button onClick={showConfirm}>Confirm</Button>
            <Row className={styles.panel}>
                <Col span={12} style={{ backgroundColor: '#fff' }}>
                    <img
                        src="https://static.www.tencent.com/uploads/2021/08/11/c9c285bfa83e1ce8222f72429249bd55.jpg!article.cover"
                        style={{ width: '100%', height: '100%' }}
                    />
                </Col>
                <Col span={12}>
                    <div style={{ width: '100%' }}>
                        <h2>系统登录</h2>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={onFinish}
                        >
                            <Form.Item
                                name="name"
                                label="账户"
                                rules={[{ required: true, message: '请输入用户名!' }]}
                            >
                                <Input placeholder="请输入用户名" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                label="密码"
                                rules={[{ required: true, message: '请输入用密码!' }]}
                            >
                                <Input
                                    type="password"
                                    placeholder="请输入密码"
                                />
                            </Form.Item>
                            <Form.Item
                                name="confirm"
                                label="确认"
                                rules={[{ required: true, message: '请确认密码!' },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue('password') === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(new Error('两次密码不一致'));
                                    },
                                })
                                ]}

                            >
                                <Input
                                    type="password"
                                    placeholder="请输入密码"
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: '100%' }}>
                                    登录
                                </Button>
                                Or <NavLink to="/login">注册</NavLink>
                            </Form.Item>
                        </Form>
                    </div>
                </Col>
            </Row>
            <ul className={styles.list}>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    )
}
export default observer(registry)