import style from './index.less';
import { PageHeader, Button, Input, Menu, Dropdown, message, Drawer, Form, Select, Switch, Popconfirm } from 'antd';
import { CloseOutlined, EllipsisOutlined } from '@ant-design/icons';
import useStore from '@/context/useStore';
import { useEffect, useState } from 'react';
import { useHistory } from 'umi';
import Editor from 'for-editor'
import { observer } from 'mobx-react-lite'
import { makeHtml, makeToc } from '@/utils/markdown';
import { publishArticle } from '@/service'
import { IArticleList } from '@/types';
const EditorDetail: React.FC = (props) => {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [settingDrawer, setSettingDrawer] = useState(false);
  const [cover, setCover] = useState('');
  const store = useStore();
  const { tagList, editorDetail } = store.editor;
  const { categoryList, articleList } = store.article;
  const [form] = Form.useForm();
  const history = useHistory();
  const [visible, setVisible] = useState(false);
  const [commentChecked, setCommentChecked] = useState(false);
  const [recommendChecked, setRecommendChecked] = useState(false);
  const [status, setStatus] = useState('');
  let id = history.location.pathname.split('/')[3];
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };
  function handleMenuClick() {
    console.log('click');
  }
  useEffect(() => {
    async function init() {
      await store.editor.getEditorDetail(id);
      await store.editor.getTagList();
      await store.editor.getFileList({ page: 1, pageSize: 12 });
      await store.article.getCategoryList();
    }
    init();
  }, [])
  useEffect(() => {
    setTitle(editorDetail.title);
    setCover(editorDetail.cover);
    setContent(editorDetail.content);
    setStatus(editorDetail.status);
  }, [editorDetail])
  const menu = (
    <Menu onClick={handleMenuClick}>
      <Menu.Item key="1">查看</Menu.Item>
      <Menu.Item key="2" onClick={showDrawer}>设置</Menu.Item>
      <Menu.Item key="3">保存草稿</Menu.Item>
      <Menu.Item key="4">删除</Menu.Item>
    </Menu>
  );
  const toolbar = {
    h1: true, // h1
    h2: true, // h2
    h3: true, // h3
    h4: true, // h4
    img: true, // 图片
    link: true, // 链接
    code: true, // 代码块
    preview: true, // 预览
    expand: true, // 全屏
    /* v0.0.9 */
    undo: true, // 撤销
    redo: true, // 重做
    save: true, // 保存
    /* v0.2.3 */
    subfield: true, // 单双栏模式
  }
  async function submit() {
    let values: IArticleList = form.getFieldsValue();
    if (!title) {
      return message.warning('请输入标题')
    };
    // 添加文章的md
    values.content = content;
     // 添加文章内容的html
    values.html = makeHtml(content);
    // 添加文章的toc
    values.toc = JSON.stringify(makeToc(values.html as string));
    // 添加文章状态
    values.status = status;
    // 添加文章的标题
    values.title = title;
    // 文章详情id
    values.id = id;
    let data = {...editorDetail, ...values}
    let result = await publishArticle(data);
    if (result.statusCode === 200) {
      message.success('文章发布成功');
      setTimeout(() => {
        history.replace('/article')
      }, 200)
    }
    
  }
  
  return <div className={style.editor}>
    <header className={style.header}>
      <PageHeader
        className="site-page-header"
        backIcon={<><Popconfirm
          title="确认关闭？如果有内容变更，请先保存。"
          onConfirm={() => history.replace('/article')}
          okText="Yes"
          cancelText="No"
        >
          <Button size='small' icon={<CloseOutlined />}></Button>
        </Popconfirm></>}
        onBack={() => null}
        title={<Input value={title} onChange={e => setTitle(e.target.value)} className={style.title} />}
        extra={[
          <div key='1'><Button onClick={submit} type='primary'>发布</Button><Dropdown overlay={menu} placement="bottomLeft">
            <Button type="link" icon={<EllipsisOutlined />}></Button>
          </Dropdown></div>
        ]}
      />
    </header>
    <main className={style.context}>
      <div className={style.content_main}>
        <section >
          <Editor
            value={content}
            toolbar={toolbar}
            onChange={value => setContent(value)}
          />
        </section>
        <Drawer width="35%" title="文章设置" placement="right" onClose={onClose} visible={visible}>
          <Form
            form={form}
          >
            <Form.Item name='summary' label="文章摘要">
              <Input.TextArea
                value={content}
                onChange={e => setContent(e.target.value)}
                placeholder="Controlled autosize"
                autoSize={{ minRows: 3, maxRows: 5 }}
              />
            </Form.Item>
            <Form.Item
              label="访问密码"
              name="password"
              rules={[{ message: 'Please input your password!' }]}
            >
              <Input.Password placeholder='输入后查看需要密码' />
            </Form.Item>
            <Form.Item
              label="付费查看"
              name="totalAmount"
              rules={[{ message: 'Please input your password!' }]}
            >
              <Input.Password placeholder='输入后需要支付费用' />
            </Form.Item>
            <Form.Item
              label="开启评论"
              name="isCommentable"
            >
              <Switch defaultChecked={commentChecked} checked={commentChecked} onChange={checked => setCommentChecked(checked)} />
            </Form.Item>
            <Form.Item
              label="首页推荐"
              name="isRecommended"
            >
              <Switch defaultChecked={commentChecked} checked={recommendChecked} onChange={checked => setRecommendChecked(checked)} />
            </Form.Item>
            <Form.Item 
              label="选择分类"
              name='category'
            >
              <Select>
                {
                  categoryList.length > 0 ? categoryList.map(item => {
                    return <Select.Option key={item.id} value={item.value}>{item.label}</Select.Option>
                  }) : null
                }
              </Select>
            </Form.Item>
            <Form.Item
              name="tags"
              label="选择标签"
              rules={[{ message: 'Please select your favourite colors!', type: 'array' }]}
            >
              <Select mode="multiple">
                {
                  tagList.length > 0 ? tagList.map(item => {
                    return <Select.Option key={item.id} value={item.value}>{item.label}</Select.Option>
                  }) : null
                }
              </Select>
            </Form.Item>
            <img src={cover} alt=""/>
            <Form.Item name="cover" label="文章封面">
                <Input type="text" placeholder="" onChange={e=>setCover(e.target.value)} />
            </Form.Item>
            <Button onClick={()=>{
                    let values = form.getFieldsValue();
                    form.setFieldsValue({...values, cover:'' })
                    setCover('');
                }}>移除</Button>
            <Button htmlType="submit">确认</Button>
          </Form>
        </Drawer>
      </div>
    </main>
  </div>
}
export default observer(EditorDetail);
