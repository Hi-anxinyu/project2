import React, { useState, useEffect } from 'react'
import styles from './index.less';
import Footer from '@/components/Footer/Footer';
import { Row, Col, Input, Select, Button, Form, Table, Space, Badge, message, Popconfirm } from 'antd';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';
import { IPageItem } from '@/types/page';
import { ReloadOutlined, } from '@ant-design/icons';
import { useHistory } from 'umi';
import BreadHeader from '@/components/Header/BreadHeader';
const page = () => {
    const store = useStore()
    const history = useHistory();
    const [flag, setFlag] = useState(false);
    const [page, setPage] = useState(1);
    const [titleArr, settitleArr] = useState(['页面管理'])
    const [pageSize, setPageSize] = useState(['8', '12', '24', '36'])
    const [selectedRowKeys, setselectedRowKeys] = useState([]);
    const [form] = Form.useForm();
    const pathname = history.location.pathname;
    useEffect(() => {
        store.page.getPageList({ page, pageSize: 12 }).then(() => {
            setFlag(flag)
        })
    }, [])
    interface Values {
        name: string,
        path: string,
        status: string
    }
    const onFinish = (values: Values) => {
        store.page.getPageList({ ...values, page, pageSize: 12 }).then(() => {
            setFlag(flag)
        })
    };
    const onReset = () => {
        form.resetFields();
    };
    const { pagelist, pagetoal } = store.page;
    //表格
    const columns = [
        {
            title: '名称',
            dataIndex: 'name',
        },
        {
            title: '路径',
            dataIndex: 'path',
        },
        {
            title: '顺序',
            dataIndex: 'order',
        },
        {
            title: '阅读量',
            key: 'views',
            render: (obj: IPageItem) => {
                return (
                    <Badge
                        count={obj.views}
                        showZero
                        style={{ backgroundColor: '#87d068' }}
                    >
                    </Badge>
                )
            }
        },
        {
            title: '状态',
            key: 'status',
            render: (obj: IPageItem) => {
                return (
                    <span>{
                        obj.status === 'publish' ? <span><Badge status="success" />已发布</span> : <span><Badge status="warning" />草稿</span>
                    }</span>
                )
            }
        },
        {
            title: '发布时间',
            dataIndex: 'publishAt',
        },
        {
            title: '操作',
            key: 'action',
            render: () => (
                <Space size="middle">
                    <a>编辑</a>
                    <a>发布</a>
                    <a>查看访问</a>
                    <a>删除</a>
                </Space>
            ),
        },
    ];
    const onSelectChange = (selectedRowKeys: any) => {
        setselectedRowKeys(selectedRowKeys)
    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    //发布
    const clickAll = async (type: string) => {
        await store.page.publishPage(selectedRowKeys, type);
        await store.page.getPageList({ page, pageSize: 12 })
        console.log(store.page.pagelist);

        message.success('操作成功');
    }
    //删除的确认和取消方法
    function cancel() {
        message.error('Click on No');
    }
    return (
        <div className={styles.container}>
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
            <main className={styles.main}>
                <div className={styles.comment_search}>
                    <Form style={{ marginLeft: '30px' }} form={form} name="control-hooks" layout="inline" onFinish={onFinish}>
                        <Row gutter={24}>
                            <Form.Item
                                name='name'
                                label="名称"
                                rules={[{ message: 'Please input your username!' }]}
                            >
                                <Input placeholder='请输入页面名称' />
                            </Form.Item>
                            <Form.Item
                                name='path'
                                label="路径"
                            >
                                <Input placeholder="请输入页面路径" />
                            </Form.Item>
                            <Form.Item label="Select" name="status" style={{ width: '120px' }}>
                                <Select>
                                    <Select.Option value='publish'>已发布</Select.Option>
                                    <Select.Option value='draft'>草稿</Select.Option>
                                </Select>
                            </Form.Item>
                        </Row>

                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" >
                                    搜索
                                </Button>
                                <Button htmlType="button" onClick={onReset} style={{ margin: '0 8px' }}>
                                    重置
                                </Button>
                            </Form.Item>
                        </Col>
                    </Form>
                </div>
                <div style={{ backgroundColor: 'white' }}>
                    <div className={styles.buttonGrounp}>
                        <div style={{ margin: 16, paddingTop: 16 }} className={styles.left}>
                            {
                                hasSelected && <div>
                                    <Button onClick={() => clickAll('publish')}>发布</Button>
                                    <Button onClick={() => clickAll('offline')}>下线</Button>
                                    <Popconfirm
                                        title="确认删除?"
                                        onConfirm={() => clickAll('delete')}
                                        onCancel={cancel}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <Button type="primary" danger ghost>删除</Button>
                                    </Popconfirm>
                                </div>
                            }
                        </div>
                        {
                            <div style={{ margin: 16, paddingTop: 16 }} className={styles.right}>
                                <Button type='primary'>+新建</Button>
                                <ReloadOutlined />
                            </div>
                        }
                    </div>
                    <Table
                        showHeader={true}
                        rowSelection={rowSelection}
                        columns={columns}
                        dataSource={pagelist}
                        rowKey='id'
                        pagination={{
                            showSizeChanger: true,
                            defaultPageSize: 8,
                            pageSizeOptions: pageSize,

                        }}
                        style={{ overflow: 'hiddle' }}
                    />
                </div>
                <Footer></Footer>
            </main>
        </div>
    )
}
export default observer(page);