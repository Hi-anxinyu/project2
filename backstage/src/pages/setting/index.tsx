import React, { useEffect, useState } from 'react'
import styles from './setting.less'
import Footer from '@/components/Footer/Footer'
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
import MonacoEditor from '@monaco-editor/react';
import BreadHeader from '@/components/Header/BreadHeader';
import { useHistory } from 'umi';
import { Tabs, Radio, Space, Form, Row, Input, Col, Button } from 'antd';
const { TabPane } = Tabs;

const setting = () => {
    let newTabIndex = 0;
    const store = useStore()
    const { settingItem } = store.setting
    const [form] = Form.useForm();
    const { TextArea } = Input
    const history = useHistory();
    const pathname = history.location.pathname;
    const [titleArr, settitleArr] = useState(['系统设置']);
    const i18n = settingItem.i18n !== undefined && JSON.parse(settingItem.i18n);

    async function initData() {
        await store.setting.getSettingDate()
    }

    const options = {
        // readOnly: true, //是否只读
        automaticLayout: true, //自动布局
        wordWrap: false, //折行展示
        scrollbar: {
            verticalScrollbarSize: 8,
            horizontalScrollbarSize: 8,
        },
        folding: true,
        renderValidationDecorations: 'on',
        selectOnLineNumbers: true,
        cursorSmoothCaretAnimation: true,
        wordWrapMinified: true,
        formatOnType: true,
        autoIndent: 'full',
        formatOnPaste: true,
        roundedSelection: true
    };

    const initialPanes = [
        {
            key: '1',
            title: <div>en</div>,
            content: <MonacoEditor
                height={450}
                language='json'
                options={options as any}
                value={JSON.stringify(i18n.en)}
            />,
        },
        {
            key: '2',
            title: <div>zh</div>,
            content: <MonacoEditor
                height={450}
                language='json'
                options={options as any}
                value={JSON.stringify(i18n.zh)}
            />,

            closable: false,
        },
    ];
    useEffect(() => {
        initData()
    }, [])
    const onFinish =async (values: any) => {
        console.log(values);
        await store.setting.setSave(values)
    }
    const [activeKey, setActiveKey] = useState(initialPanes[0].key)
    const [panes, setPanes] = useState(initialPanes)
    const onChange = (activeKey: any) => {
        setActiveKey(activeKey);
        console.log(activeKey);
    };
    const onEdit = (targetKey: any, action: string | number) => {
        // action = targetKey;
        // console.log(action,targetKey);
    };
    const add = () => {
        const activeKey = `newTab${newTabIndex++}`;
        const newPanes = [...panes];
        newPanes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey } as any);
        setPanes(newPanes);
        setActiveKey(activeKey)
    };
    const remove = (targetKey: any) => {

        let newActiveKey = activeKey;
        let lastIndex = 0;
        panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const newPanes = panes.filter(pane => pane.key !== targetKey);
        if (newPanes.length && newActiveKey === targetKey) {
            if (lastIndex >= 0) {
                newActiveKey = newPanes[lastIndex].key;
            } else {
                newActiveKey = newPanes[0].key;
            }
        }
        setPanes(newPanes);
        setActiveKey(activeKey)
    }

    return (
        <div className={styles.setting}>
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
            <div className={styles.setting_box}>
                <Tabs tabPosition={'left'} size={'small'} style={{ minHeight: '500px', }} >
                    <TabPane tab="系统设置" key="1" >
                        <div className={styles.setting_box_content}>
                            <Form 
                                initialValues={
                                {
                                    adminSystemUrl: settingItem.adminSystemUrl,
                                    systemUrl: settingItem.systemUrl,
                                    systemTitle: settingItem.systemTitle,
                                    systemLogo: settingItem.systemLogo,
                                    systemFavicon: settingItem.systemFavicon,
                                    systemFooterInfo: settingItem.systemFooterInfo,

                                }
                            } form={form} name="control-hooks" layout='vertical' onFinish={onFinish}>
                                <Form.Item
                                    key='adminSystemUrl'
                                    label="系统地址"
                                    name='adminSystemUrl'
                                    initialValue={settingItem.adminSystemUrl}
                                >
                                    <Input placeholder='请输入系统地址' />
                                </Form.Item>
                                <Form.Item
                                    key='systemUrl'
                                    name='systemUrl'
                                    initialValue={settingItem.systemUrl}
                                    label="后台地址">
                                    <Input placeholder="请输入后台地址" />
                                </Form.Item>
                                <Form.Item
                                    key='systemTitle'
                                    initialValue={settingItem.systemTitle}
                                    name='systemTitle'
                                    label="系统标题">
                                    <Input placeholder="请输入系统标题" />
                                </Form.Item>

                                <Form.Item
                                    key='systemLogo'
                                    initialValue={settingItem.systemLogo}
                                    name='systemLogo'
                                    label="Logo">
                                    <Input placeholder="请输入Logo地址" />
                                </Form.Item>
                                <Form.Item
                                    key='systemFavicon'
                                    initialValue={settingItem.systemFavicon}
                                    name='systemFavicon'
                                    label="Favicon">
                                    <Input placeholder="请输入Favicon地址" />
                                </Form.Item>
                                <Form.Item
                                    key='systemFooterInfo'
                                    initialValue={settingItem.systemFooterInfo}
                                    name='systemFooterInfo'
                                    label="页脚信息">
                                    <TextArea rows={10} placeholder="请输入页脚信息" />
                                </Form.Item>
                                <Form.Item>
                                    <Button type="primary" htmlType="submit" >
                                        保存
                                    </ Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </TabPane>
                    <TabPane tab="国际化设置" key="2">
                        <div className={styles.setting_box_content}>
                            <div className={styles.setting_box_content_tab}>
                                <Tabs
                                    type="editable-card"
                                    onChange={onChange}
                                    activeKey={activeKey}
                                    onEdit={onEdit}
                                >
                                    {panes.map(pane => (
                                        <TabPane tab={pane.title} key={pane.key} closable={pane.closable}>
                                            {pane.content}
                                        </TabPane>
                                    ))}
                                </Tabs>
                                <div style={{padding:'20px 0'}}>
                                    <Button type="primary" >保存</Button>
                                </div>
                            </div>
                        </div>
                    </TabPane>
                    <TabPane tab="SEO设置" key="3">
                        <div className={styles.setting_box_content}>

                        </div>
                    </TabPane>
                    <TabPane tab="数据统计" key="4">
                        <div className={styles.setting_box_content}>

                        </div>
                    </TabPane>
                    <TabPane tab="OSS设置" key="5">
                        <div className={styles.setting_box_content}>

                        </div>
                    </TabPane>
                    <TabPane tab="SMTP服务" key="6">
                        <div className={styles.setting_box_content}>

                        </div>
                    </TabPane>
                </Tabs>
            </div>
            <Footer />
        </div>
    )
}
export default observer(setting)