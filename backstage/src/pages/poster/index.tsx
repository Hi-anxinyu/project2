import React, { useState } from 'react'
import BreadHeader from '@/components/Header/BreadHeader';
import { useHistory } from 'umi';
import styles from './index.less';
import { message } from 'antd';
import UpLoad from '@/components/file/UpLoad'
import Footer from '@/components/Footer/Footer';
import Form from '@/components/poster/From'
import { Table } from 'antd';
import { FormInstance } from 'antd/lib/form';
const poster = () => {
    const history = useHistory();
    const pathname = history.location.pathname;
    const [titleArr, settitleArr] = useState(['海报管理']);
    const [dataSource,setdataSource] = useState([]);
    const upload = {
        name: 'file',
        // multiple: true,
        action: '/api/file/upload',
        onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
                console.log(1);

            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
                console.log(2);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
                console.log(3);

            }
        },
        onDrop(e: { dataTransfer: { files: any; }; }) {
            console.log('Dropped files', e.dataTransfer.files);
            console.log(e.dataTransfer.files);

        },
        itemRender() {

        },
        showUploadList: true,
    };

    return (
        <div className={styles.poster}>
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
            <main className={styles.main}>
                <div className={styles.file_content}>
                    <UpLoad upload={upload} />
                    <Form></Form>
                    <Table
                        dataSource={dataSource}
                    />
                </div>
                <Footer></Footer>
            </main>

        </div>
    )
}
export default poster