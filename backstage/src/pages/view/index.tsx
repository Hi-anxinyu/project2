import React, { useState } from 'react'
import style from './index.less';
import BreadHeader from '@/components/Header/BreadHeader';
import { useHistory } from 'umi';
const view = () =>{
    const history = useHistory();
    const pathname = history.location.pathname;
    const [titleArr, settitleArr] = useState(['访问统计']);
    return (
        <div className={style.view}>
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
             view
        </div>
    )
}
export default view