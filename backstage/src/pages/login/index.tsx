import { useHistory, useLocation } from 'umi';
import styles from './index.less';
import { Row, Col, Form, Input, Button, message } from 'antd';
import userStore from '@/context/useStore';
import { ILoginForm } from '@/types';
import { observer } from 'mobx-react-lite';
interface ILocationQuery {
    query: {
        from: string
    }
}
const login = () => {
    const history = useHistory();
    const store = userStore();
    const location = useLocation();
    const onFinish = async (values: ILoginForm) => {
        let result = await store.user.login(values);
        if (Boolean(result)) {
            localStorage.token = result.token;
            localStorage.user = JSON.stringify(result);
            let redirect = '/';
            if ((location as unknown as ILocationQuery).query.from) {
                redirect = decodeURIComponent((location as unknown as ILocationQuery).query.from)
            }
            history.replace(redirect);
        } else {
            message.info('账号或密码错误')
        }
    };
    return (
        <div className={styles.login_box}>
            <Row className={styles.panel}>
                <Col span={12} style={{backgroundColor:'#fff'}}>
                    <img
                        src="https://gw.alipayobjects.com/mdn/rms_08e378/afts/img/A*P0S-QIRUbsUAAAAAAAAAAABkARQnAQ"
                        style={{ width: '100%', height: '100%' }}
                    />
                </Col>
                <Col span={12}>
                    <div style={{ width: '100%' }}>
                        <h2>系统登录</h2>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={onFinish}
                        >
                            <Form.Item
                                name="name"
                                label="账户"
                                rules={[{ required: true, message: '请输入用户名!' }]}
                            >
                                <Input placeholder="请输入用户名" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                label="密码"
                                rules={[{ required: true, message: '请输入用密码!' }]}
                            >
                                <Input
                                    type="password"
                                    placeholder="请输入密码"
                                />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button" style={{ width: '100%' }}>
                                    登录
                                </Button>
                                Or <a href="">register</a>
                            </Form.Item>
                        </Form>
                    </div>
                </Col>
            </Row>
            <ul className={styles.list}>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    )
}
export default observer(login)