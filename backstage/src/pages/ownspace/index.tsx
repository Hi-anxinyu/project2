import React, { useEffect } from 'react';
import styles from './index.less';
import Footer from '@/components/Footer/Footer';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import { Row, Col, Card, List, Tabs, Avatar, Form, Input, Button, message } from 'antd';
import { updateUserInfo } from '@/service'
const Ownspace = () => {
    const store = useStore();
    useEffect(() => {
        store.article.getArticleList({});
        store.article.getCategoryList();
        store.comment.getCommentList({ page: 1 });
        store.editor.getTagList();
        store.file.getFileList({page:1})
    }, []);
    const data = [
        `累计发表了 ${store.article.total} 篇文章`,
        `累计创建了 ${store.article.categoryList.length} 个分类`,
        `累计创建了 ${store.comment.total} 个标签`,
        `累计上传了 ${store.tag.tagtotal} 个文件`,
        `累计获得了 ${store.file.total} 个评论`,
    ];
    const { TabPane } = Tabs;
    const onFinish = async (values: { name: string, email: string }) => {
        let res = await updateUserInfo(values);
        if (res.statusCode === 201) {
            store.user.userInfo = res.data
            localStorage.user = JSON.stringify(res.data);
            message.success('用户信息已保存');
        } else {
            message.success(res.msg);
        }
    };
    const [form] = Form.useForm();
        const onFinish1 = (values: any) => {
            console.log('Received values of form: ', values);
        };
 
    return (
        <div>
            <header></header>
            <main className={styles.container}>
                <Row gutter={16}>
                    <Col span={12}>
                        <List
                            size="large"
                            style={{ backgroundColor: "#fff" }}
                            header={<div>系统概览</div>}
                            bordered
                            dataSource={data}
                            renderItem={item => <List.Item>{item}</List.Item>}
                        />
                    </Col>
                    <Col span={12} >
                        <Card
                            title='个人资料'
                            style={{ marginTop: 0 }}
                        >
                            <Tabs defaultActiveKey="1">
                                <TabPane tab="基本设置" key="1">
                                    <Row>
                                        <Col span={24} style={{ textAlign: 'center' }}>
                                            <Avatar size={64}
                                                style={{ textAlign: 'center' }}
                                                src={store.user.userInfo.avatar || localStorage.user ? JSON.parse(localStorage.user).avatar : ''}
                                            />
                                        </Col>
                                    </Row>
                                    <Row style={{ paddingTop: 20 }}>
                                        <Col span={24}>
                                            <Form
                                                onFinish={onFinish}
                                            >
                                                <Form.Item
                                                    label="用户名"
                                                    name="name"
                                                    rules={[{ required: true, message: '用户名不能为空' }]}
                                                >
                                                    <Row>
                                                        <Col span={1}></Col>
                                                        <Col span={24}>
                                                            <Input placeholder="输入用户名" />
                                                        </Col>
                                                    </Row>
                                                </Form.Item>
                                                <Form.Item
                                                    label="邮箱"
                                                    name='email'
                                                    rules={[{ required: true, message: '邮箱不能为空' }]}
                                                >
                                                    <Row>
                                                        <Col span={1}></Col>
                                                        <Col span={23}>
                                                            <Input placeholder="输入邮箱" />
                                                        </Col>
                                                    </Row>
                                                </Form.Item>
                                                <Form.Item >
                                                    <Button type="primary" htmlType="submit">保存</Button>
                                                </Form.Item>
                                            </Form>
                                        </Col>
                                    </Row>
                                </TabPane>
                                <TabPane tab="更新密码" key="2">
                                    <Form
                                        form={form}
                                        name="register"
                                        onFinish={onFinish1}
                                    >
                                        <Form.Item
                                            name="password"
                                            label="原密码"
                                            rules={[
                                                {
                                                    required: true, message: '请输入原密码',
                                                },
                                            ]}
                                        >
                                            <Input.Password />
                                        </Form.Item>
                                        <Form.Item
                                            name="New Password"
                                            label="新密码"
                                            rules={[
                                                {
                                                    required: true, message: '请输入新密码',
                                                },
                                                ({getFieldValue})=>({
                                                    validator(_,value){
                                                        if(value.length>6){
                                                            return Promise.resolve();
                                                        }
                                                        return Promise.reject(new Error('密码长度不够6位数'));
                                                    }
                                                })
                                            ]}
                                        >
                                            <Input.Password />
                                        </Form.Item>
                                        <Form.Item
                                            name="confirm" label="确认密码"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: '请确认密码',
                                                },
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        if (!value || getFieldValue('New Password') === value) {
                                                            return Promise.resolve();
                                                        }
                                                        return Promise.reject(new Error('两次密码不一致'));
                                                    },
                                                }),
                                            ]}
                                        >
                                            <Input.Password />
                                        </Form.Item>
                                        <Form.Item>
                                            <Button type="primary" htmlType='submit'>更新</Button>
                                        </Form.Item>
                                    </Form>
                                </TabPane>
                            </Tabs>
                        </Card>
                    </Col>
                </Row>
                <Footer></Footer>
            </main>
        </div>
    )
}

export default observer(Ownspace);