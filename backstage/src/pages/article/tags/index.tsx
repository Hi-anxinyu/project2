import BreadHeader from "@/components/Header/BreadHeader";
import React, { useEffect, useState } from "react";
import { useHistory } from 'umi';
import style from './tags.less'
import {Modal, Col, Card, Form, Input, Button, Checkbox, Row } from 'antd'
import useStore from '@/context/useStore'
import { observer } from 'mobx-react-lite'
import { ICategoryList } from "@/types";
const Tags: React.FC = (props) => {
  const store = useStore();
  const { tagsList } = store.Tags;
  const [isfalg, setIsfalg] = useState(false);
  const history = useHistory();
  const pathname = history.location.pathname;
  const [titleArr, settitleArr] = useState(['文章管理','标签管理'])
  const [id, setId] = useState('');
  const [form] = Form.useForm();
  const reg = /\w*[a-zA-Z]{3,7}\b/; // 正则匹配纯英文
  useEffect(() => {
    store.Tags.getTagsList()
  }, [])
  const onFinish = async (values: any) => {
    await store.Tags.addTagsList({...values});
    getinit()
  };
  function getinit() {
    setTimeout(async () => {
      await store.Tags.getTagsList();
    }, 200)
  }
  function showCategory(item: ICategoryList) {
    form.setFieldsValue({
      label: item.label,
      value: item.value
    })
    setIsfalg(true)
    setId(item.id)
  }
  
  function cancelCategory(){
    form.setFieldsValue({
      label: '',
      value: ''
    })
    setIsfalg(false)
  }
  async function deleteCategory() {
    form.setFieldsValue({
      label: '',
      value: ''
    })
    await store.Tags.removeTagsList(id);
    getinit()
    setIsfalg(false)
  }
  return <div className={style.category}>
    <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
    <main>
      <Row>
        <Col flex={2} style={{ padding: '0 8px' }}>
          <Card title="添加标签" >
            <Form
            form={form}
              name="normal_login"
              className="login-form"
              initialValues={{ remember: true }}
              onFinish={onFinish}
            >
              <Form.Item
                name="label"
                rules={[{ required: true, message: '输入标签名称' }]}
              >
                <Input placeholder="输入标签名称"/>
              </Form.Item>
              <Form.Item
                name="value"
                rules={[{ required: true, pattern: new RegExp(reg), message: '输入标签值（请输入英文作为路由使用）' }]}
              >
                <Input
                  type="input"
                  placeholder="输入标签值（请输入英文作为路由使用）"
                  value={'dasdf'}
                />
              </Form.Item>
              <Form.Item>
                <div className={style.category_button}>
                  <div className={style.category_btn_box}>
                    <Button type="primary" htmlType="submit" className="login-form-button">保存</Button>
                    <Button style={{display: isfalg ? 'block' : 'none'}} onClick={cancelCategory} type="dashed" >返回添加</Button>
                  </div>
                  <Button onClick={deleteCategory} style={{display: isfalg ? 'block' : 'none'}} type="primary" danger ghost>删除</Button>
                </div>
              </Form.Item>
            </Form>
          </Card>
        </Col>
        <Col flex={3} style={{ padding: '0 8px' }}>
          <Card title="所有标签" >
            <ul className={style.categorylist}>
              {
                tagsList.length > 0 ? tagsList.map(item => {
                  return <li
                    onClick={() => showCategory(item)}
                    className={style.categorylist_item}
                    key={item.id}
                  ><a><span>{item.value}</span></a></li>
                }) : null
              }
            </ul>
          </Card>
        </Col>
      </Row>
    </main>
  </div>
}
export default observer(Tags);