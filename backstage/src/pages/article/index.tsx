import React, { useEffect, useState } from 'react'
import { Table, Form, Row, Col, Input, Button, Select, Tag, Divider, Badge, Spin, Popconfirm, message, Pagination } from 'antd';
import style from './index.less';
import './index.less';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite'
import { IArticleList, ITags } from '@/types';
import { changeArticleRecommended, deleteRaticle } from '@/service';
import Footer from '@/components/Footer/Footer'
import { Link, useHistory } from 'umi';
import PlusOutlined from '@ant-design/icons/lib/icons/PlusOutlined';
import Loading3QuartersOutlined from '@ant-design/icons/lib/icons/Loading3QuartersOutlined';
import BreadHeader from '@/components/Header/BreadHeader';
import PageZhCn from '@/components/PageZhCn';
const { Option } = Select;
const COLORS = [
	'#e21400', '#91580f', '#f8a700', '#f78b00',
	'#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
	'#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];
let Categorycolor = COLORS[Math.round(Math.random() * 10)];
let Tagscolor = COLORS[Math.round(Math.random() * 10)]
const article = () => {
	const history = useHistory();
	const store = useStore();
	const [selectedRowKeys, setSelectedRowKeys] = useState([]);
	const [loading, setLoading] = useState(false);
	const { articleList, categoryList, total } = store.article;
	const [page, setpage] = useState(1);
	const [title, setTitle] = useState('');
	const pathname = history.location.pathname;
	const [titleArr, settitleArr] = useState(['文章管理'])
	useEffect(() => {
		async function initState() {
			await store.article.getArticleList({ page, pageSize: 12, title: '' });
			await store.article.getCategoryList();
		}
		initState();
	}, [page])
	useEffect(() => {
		articleList.length > 0 ? setLoading(false) : setLoading(true)
	}, [articleList])
	const columns: any = [
		{
			title: '标题',
			width: 150,
			dataIndex: 'title',
			id: 'title',
			fixed: 'left',
		},
		{
			title: '状态',
			width: 100,
			dataIndex: 'status',
			id: 'status',
			render: (status: string) => {
				{
					if (status === 'publish') {
						return <><Badge status="success" />已发布</>
					} else {
						return <><Badge status='warning' />草稿</>
					}
				}
			}
		},
		{
			title: '分类',
			dataIndex: 'category',
			id: 'category',
			width: 100,
			render: (category: ITags) => {
				if (category) {
					return <Tag color={Categorycolor} key={category.id}>{category.label}</Tag>
				} else {
					return <></>
				}
			}
		},
		{
			title: '标签',
			dataIndex: 'tags',
			id: 'tags',
			width: 150,
			render: (tags: ITags[]) => {
				if (tags.length > 0) {
					return <>
						{
							tags.map(item => {
								return <Tag color={Tagscolor} key={item.id}>{item.label}</Tag>
							})
						}
					</>
				}
			}
		},
		{
			title: '阅读量',
			dataIndex: 'views',
			id: 'views',
			width: 100,
			render: (views: string) => {
				return <Badge
					className="site-badge-count-109"
					count={views}
					showZero
					style={{ backgroundColor: '#52C41A' }}
				/>
			}
		},
		{
			title: '喜欢数',
			dataIndex: 'likes',
			id: 'likes',
			width: 100,
			render: (likes: string) => {
				return <Badge
					count={likes}
					showZero
					style={{ backgroundColor: '#EB2F96' }}
				/>
			}
		},
		{
			title: '发布时间',
			dataIndex: 'publishAt',
			id: 'publishAt',
			width: 150,
		},
		{
			title: '操作',
			id: 'operation',
			fixed: 'right',
			width: 254,
			render: (text: IArticleList, record: any) => (
				<span className={style.action}>
					<Link to={`/article/editor/${text.id}`} className={style.space}>编辑</Link>
					<Divider type='vertical' />
					<a onClick={() => changeRecommended(text.id, true)}>首焦推荐</a>
					<Divider type='vertical' />
					<a>查看访问</a>
					<Divider type='vertical' />
					<a onClick={() => deleteArticle(text.id)}>删除</a>
				</span>
			),
		},
	];
	const AdvancedSearchForm = () => { // 表单
		const [form] = Form.useForm();
		const getFields = () => {
			const children = [
				<Col offset={1} span={5} key='1'>
					<Form.Item
						name={'title'}
						label={`标题`}
						rules={[
							{
								message: 'Input something!',
							},
						]}
					>
						<Input placeholder="请输入文章标题" />
					</Form.Item>
				</Col>,
				<Col offset={1} span={5} key='2'>
					<Form.Item
						name="status"
						label="状态"
						hasFeedback
						rules={[{ message: 'Please select your country!' }]}
					>
						<Select placeholder="请选择文件状态">
							<Option value="publish">已发布</Option>
							<Option value="draft">草稿</Option>
						</Select>
					</Form.Item>
				</Col>,
				<Col offset={1} span={5} key='3'>
					<Form.Item
						name="category"
						label="分类"
						hasFeedback
						rules={[{ message: 'Please select your country!' }]}
					>
						<Select placeholder="文章分类">
							{
								categoryList.length > 0 && categoryList.map(item => {
									return <Option key={item.id} value={item.value}>{item.label}</Option>
								})
							}
						</Select>
					</Form.Item>
				</Col>,
			];
			return children;
		};

		const onFinish = (values: { category?: string, status?: string, title?: string }) => {
			const { category, status, title } = values;
			gitinit(title, status, category)
		};

		return (
			<Form
				style={{ padding: '24px 12px', backgroundColor: '#fff' }}
				form={form}
				name="advanced_search"
				className="ant-advanced-search-form"
				onFinish={onFinish}
			>
				<Row style={{ height: '40px' }} gutter={[16, 16]} justify="start">{getFields()}</Row>
				<Row>
					<Col span={24} style={{ textAlign: 'right' }}>
						<Button type="primary" htmlType="submit">
							搜索
						</Button>
						<Button
							style={{ margin: '0 8px' }}
							onClick={() => {
								form.resetFields();
							}}
						>
							重置
						</Button>
					</Col>
				</Row>
			</Form>
		);
	};
	const onSelectChange = (selectedRowKeys: any) => {
		console.log('selectedRowKeys changed: ', selectedRowKeys);
		setSelectedRowKeys(selectedRowKeys)
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
	};
	function gitinit(title?: string, status?: string, category?: string) {
		setLoading(true)
		setTimeout(async () => {
			await store.article.getArticleList({ page, pageSize: 12, title, status, category });
			setLoading(false)
		}, 200)
	}
	async function changeStatus(status: string) { // 改变文章状态
		await store.article.changeArticleStatus({ id: selectedRowKeys, status });
		gitinit()
	}

	async function changeRecommendeds(isRecommended: boolean, id?: string[]) { // 改变首焦 (多个)
		await store.article.changeArticleRecommended({ id: selectedRowKeys, isRecommended });
		gitinit()
	}

	async function changeRecommended(id: string, isRecommended: boolean,) { // 改变首焦 (单个)
		// await store.article.changeArticleRecommended({id, isRecommended});
		let result = await changeArticleRecommended({ id, isRecommended });
		if (result.statusCode === 200) {
			gitinit()
			message.success('操作成功')
		}

	}
	async function deleteArticles() { // 删除文章 (多个)
		await store.article.deleteRaticle(selectedRowKeys);
		gitinit()
	}
	async function deleteArticle(id: string) { // 删除文章 (多个)
		let result = await deleteRaticle(id);
		if (result.statusCode === 200) {
			gitinit()
			message.success('操作成功')
		}
	}
	async function changePage(page: number, pageSize?: number) {
		await store.article.getArticleList({ page, pageSize });
	}
	return (
		<div className={style.article}>
			<BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
			<div className={style.wraper}>
				<AdvancedSearchForm />
				<div className={style.table_box}>
					<div style={{ marginBottom: 16, height: '32px', position: 'relative' }}>
						{
							selectedRowKeys.length > 0 ? <div style={{ width: '60%' }}>
								<Button style={{ marginRight: '8px' }} onClick={() => changeStatus('publish')}>发布</Button>
								<Button style={{ marginRight: '8px' }} onClick={() => changeStatus('draft')}>草稿</Button>
								<Button style={{ marginRight: '8px' }} onClick={() => changeRecommendeds(true)}>首页推荐</Button>
								<Button style={{ marginRight: '8px' }} onClick={() => changeRecommendeds(false)}>撤销首焦</Button>
								<Popconfirm
									title="确认删除？"
									onConfirm={deleteArticles}
									okText="Yes"
									cancelText="No"
								>
									<Button style={{ marginRight: '8px' }} danger>删除</Button>
								</Popconfirm>,
							</div> : null
						}
						<div style={{ position: 'absolute', right: '0px', top: '0px' }}>
							<Button icon={<PlusOutlined />} type='primary' onClick={() => history.push('/article/editor')}>新建</Button>
							<Button icon={<Loading3QuartersOutlined />} type='text' onClick={() => gitinit()}></Button>
						</div>
					</div>
					{
						articleList.length > 0 ? <Spin spinning={loading}><PageZhCn>
							<Table
								rowSelection={rowSelection}
								columns={columns}
								dataSource={articleList}
								scroll={{ x: 1500 }}
								pagination={{ total, defaultPageSize: 12, showSizeChanger: true, pageSizeOptions: ['8', '12', '24', '36'], onChange: changePage}}
								rowKey='id' />
						</PageZhCn>
						</Spin> : null
					}
				</div>
				<Footer></Footer>
			</div>
		</div>
	)
}
export default observer(article);