import React, { useEffect, useState } from 'react'
/*MobX仓库引入*/
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore';
import Froms from '@/components/mail/From'
import styles from './mail.less'
import { IValues } from '@/types/mail'
import { Space, Table, Tooltip, Button, Popconfirm, message } from 'antd';
import Footer from '@/components/Footer/Footer';
import BreadHeader from '@/components/Header/BreadHeader';
import { useHistory } from 'umi';
import { IMailItem } from '@/types';
const Mail = () => {
    const store = useStore();
    const [flag, setFlag] = useState(false);
    const history = useHistory();
    const pathname = history.location.pathname;
    const [titleArr, settitleArr] = useState(['邮件管理']);
    const [page, setpage] = useState(1);
    const [pageSize, setPageSize] = useState(['8', '12', '24', '36'])
    const [selectedRowKeys, setSelectedRowKeys] = useState([])
    useEffect(() => {
        store.mail.getSmtpList({})
    }, [page])
    //获取表格的数据源
    const { mailList } = store.mail;
    //表格
    const columns = [
        {
            title: '发件人',
            dataIndex: 'from',
        },
        {
            title: '收件人',
            dataIndex: 'to',
        },
        {
            title: '主题',
            dataIndex: 'subject',
        },
        {
            title: '发布时间',
            dataIndex: 'createAt',
        },
        {
            title: '操作',
            key: 'action',
            textAlign: 'center',
            render: (text: IMailItem) => (
                <Space size="middle">
                    <Button
                        type="primary"
                        danger ghost
                        onClick={() => {
                            let singleArr:Array<string> = [];
                            singleArr.push(text.id);
                            store.mail.deleteMail(singleArr);
                            singleArr.length = 0;
                        }}
                    >
                        删除</Button>
                </Space>
            )
        },
    ];
    //回复的弹出框
    const [visible, setVisible] = useState(false)
    const showModal = () => {
        setVisible(true);
    };
    //成功时
    const hideModal = () => { 
        setVisible(false);
    };
    //From表单的事件,点击搜索
    const onFinish = (values: IValues) => {
        store.mail.getSmtpList(values)
    };
    const onSelectChange = (selectedRowKeys: any) => {
        setSelectedRowKeys(selectedRowKeys)
        console.log(selectedRowKeys);

    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    //删除的确认和取消方法
    function cancel() {
        message.error('Click on No');
    }
    function deleteMail() {
        store.mail.deleteMail(selectedRowKeys);
    }
    const hasSelected = selectedRowKeys.length > 0;
    return (
        <div className={styles.mail}>
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
            <div className={styles.mail_from}>
                <Froms onFinish={onFinish} />
            </div>
            <div className={styles.mail_table}>
                {
                    hasSelected &&
                    <Popconfirm
                        title="确认删除?"
                        onConfirm={() => deleteMail()}
                        onCancel={cancel}
                        okText="确认"
                        cancelText="取消"
                    >
                        <Button type="primary" danger ghost style={{ marginRight: 12 }}>删除</Button>
                    </Popconfirm>
                }
                <div className={styles.mail_table_button_icon}>
                    <Tooltip placement="top" title='刷新'>
                        <i className={'iconfont icon-shuaxin'}></i>
                    </Tooltip>
                </div>
                <Table
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={mailList}
                    rowKey='id'
                    pagination={{
                        showSizeChanger: true,
                        defaultPageSize: 8,
                        pageSizeOptions: pageSize,
                    }}
                />
            </div>
            <Footer></Footer>
        </div>
    )
}
export default observer(Mail)