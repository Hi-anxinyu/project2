import { Pagination } from 'antd';
import { observer } from "mobx-react-lite"
import useStore from '@/context/useStore';
import Header from "@/components/knowledge/Header"
import Footer from "@/components/Footer/Footer"
import Form1 from "@/components/knowledge/Form"
import { useEffect, useState } from 'react';
import styles from "./index.less"
import Draw from '@/components/knowledge/Draw';
import Drawers from '@/components/knowledge/Drawers';

const knowledge = () => {
    const store = useStore();
    //传的参数
    const [page, setpage] = useState(1);
    const [pageSize, setpageSize] = useState(12);
    const [params, setParams] = useState({});
    const { knowledgeList } = store.knowledge;
    console.log(knowledgeList);
    useEffect(() => {
        store.knowledge.getKonwledgeList(page, pageSize, params)
    }, [page, pageSize, params])

    return (
        <div className={styles._3nJJ5Sx6PbHhFVGf3_Awr6}>
            <Header />
            <main>
                <div className={styles.box123}>
                    {/* input框和select框和按钮 */}
                    <Form1 />
                    <div className={styles.box456}>
                        {/* 抽屉 */}
                        <Drawers />
                        {/* 渲染的数据 */}
                        <Draw knowledgeList={knowledgeList} />
                        {/* 分页 */}
                        <div className={styles.box235}>
                            <Pagination
                                total={store.knowledge.knowledgeCount}
                                showTotal={total => `共 ${store.knowledge.knowledgeCount} 条`}
                                defaultPageSize={12}
                                defaultCurrent={1}
                                showSizeChanger={true}
                                pageSizeOptions={["8", "12", "24", "36"]}
                            />
                        </div>
                    </div>
                </div>
                <Footer />
            </main>
        </div>
    )
}
export default observer(knowledge);