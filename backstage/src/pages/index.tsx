import React, { useEffect, useState } from 'react';
import { Breadcrumb, Typography, Card, Col, Row, List,Button, Badge, Popover, Modal, Input ,message} from 'antd';
import styles from './index.less';
import { NavLink } from 'react-router-dom';
import useStore from '@/context/useStore';
import { observer } from 'mobx-react-lite';
import Footer from '@/components/Footer/Footer';
const IndexPage = () => {
    const { TextArea } = Input;
    const store = useStore();
    const { Title, Text } = Typography;
    //定义导航状态
    const [navlist, setnavlist] = useState([
        {
            path: '/article',
            text: '文章管理',
        },
        {
            path: '/comment',
            text: '评论管理',
        },
        {
            path: '/file',
            text: '文件管理',
        },
        {
            path: '/user',
            text: '用户管理',
        },
        {
            path: '/view',
            text: '访问管理',
        },
        {
            path: '/setting',
            text: '系统设置',
        },
    ]);
    //调用接口 发送请求
    useEffect(() => {
        store.comment.getCommentList({ page: 1, pageSize: 6, });
        store.article.getArticleList({ page: 1, pageSize: 6, title: '' });
    }, [1]);
    //获取mobx状态
    let commentList = store.comment.commentList, { articleList } = store.article, { userInfo } = store.user;
    // 划過文章、查看内容字体 预览页面  查看内容
    const text = (word: string) => <span>{word}</span>;
    const content = (hostId: string) => {
        return (
            <iframe
                src={`https://creationadmin.shbwyz.com/creationad.shbwyz.co/article/${hostId}`}
                className='preview'
            ></iframe>
        )
    };
    const content1 = (content: string) => <div>{content}</div>;
    //评论通过操作
    async function agreeOrRefuse(type: string, id: string) {
        if (type === 'agree') {
            await store.comment.setAction({ id, pass: true });
            store.comment.getCommentList({ page: 1, pageSize: 6, });
            message.success('评论已通过')
        } else if (type === 'refuse') {
            await store.comment.setAction({ id, pass: false });
            store.comment.getCommentList({ page: 1, pageSize: 6, });
            message.success('评论已拒绝')
        } else if (type === 'del') {
            await store.comment.setDelete(id)
            store.comment.getCommentList({ page: 1, pageSize: 6, });
            message.success('评论已删除');
        }
    }
    //回复的弹出框
    const [visible, setVisible] = useState(false)
    const [val, setVal] = useState<string>('')
    //失败时候
    const showModal = () => {
        setVisible(true);
    };
    //成功时
    const hideModal = () => {
        setVisible(false);
    };
    const ChangeVal = ({ target: { value } }: any) => {
        setVal(value)
        console.log(val, value);
    };
    return (
        <>
            <header className={styles.header}>
                <Breadcrumb>
                    <Breadcrumb.Item>工作台</Breadcrumb.Item>
                </Breadcrumb>
                <div className={styles.interval}></div>
                <Title>您好，{userInfo.name || (localStorage.user ? JSON.parse(localStorage.user).name : '')}</Title>
                <Text>您的身份: {userInfo.role || (localStorage.user ? JSON.parse(localStorage.user).role : '')}</Text>
                <div className={styles.interval}></div>
            </header>
            <main className={styles.content}>
                <div className="site-card-wrapper">
                    <Row gutter={16}>
                        <Col span={24}>
                            <Card title="快速导航" bordered={false} className={styles.navbox}>
                                {
                                    navlist.map(item => {
                                        return (
                                            <NavLink key={item.path} to={item.path}>{item.text}</NavLink>
                                        )
                                    })
                                }
                            </Card>
                        </Col>
                    </Row>
                </div>
                {/* 文章 */}
                <Card title="最新文章" className='articlebox' extra={<NavLink to="/article">更多文章</NavLink>}>
                    {
                        articleList && articleList.map(item => {
                            return (
                                <Card.Grid style={{ width: '33.3333%', textAlign: 'center' }} key={item.id}>
                                    <NavLink to={`/article/editor/${item.id}`} className='link'>
                                        <img src={item.cover} alt="文章封面" />
                                        <p className={styles.text}>{item.title}</p>
                                    </NavLink>
                                </Card.Grid>
                            )
                        })
                    }
                </Card>
                {/*最新评论*/}
                <Card title="最新评论"
                    className='commentbox'
                    extra={<NavLink to="/commemt">全部评论</NavLink>}
                >
                    <List
                        bordered={false}
                        dataSource={commentList}
                        renderItem={item => (
                            <List.Item
                                actions={[
                                    <a key="list-loadmore-agree" onClick={() => agreeOrRefuse('agree', item.id)}>通过</a>,
                                    <a key="list-loadmore-refuse" onClick={() => agreeOrRefuse('refuse', item.id)}>拒绝</a>,
                                    <a key="list-loadmore-reply"
                                        onClick={showModal}
                                    >回复</a>,
                                    <Popover
                                        content={
                                            <div>
                                                <Button size='small'>取消</Button>
                                                <Button type='primary' size='small' onClick={() => agreeOrRefuse('del', item.id)}>确认</Button>
                                            </div>
                                        }
                                        title="确定删除这条评论"
                                        trigger="hover"
                                    >
                                        <a key="list-loadmore-del">
                                            删除</a>
                                    </Popover>
                                ]}
                            >
                                {item.name} 在 <em></em>
                                <a
                                    href={`https://creationadmin.shbwyz.com/creationad.shbwyz.co/article/${item.hostId}`}
                                    target="_blank" rel="noreferrer"
                                >
                                    <Popover placement="right" title={text('预览页面')} content={content(item.hostId)} trigger="hover">
                                        文章
                                    </Popover>
                                </a>
                                <span>  评论</span>
                                <Button style={{ paddingLeft: 5 }} type="link">
                                    <Popover placement="top" title={text('品论详情-原始内容')} content={content1(item.content)} trigger="hover">
                                        查看内容
                                    </Popover>
                                </Button>
                                <Badge status={item.pass ? "success" : "warning"} text={item.pass ? "通过" : "未通过"} />
                            </List.Item>
                        )}
                    />
                </Card>
                <Modal
                    title="回复评论"
                    visible={visible}
                    onOk={hideModal}
                    onCancel={hideModal}
                    okText="确认"
                    cancelText="取消"
                >
                    <div className='modal_mask' style={{ height: '150px' }}>
                        <TextArea
                            value={val}
                            onChange={ChangeVal}
                            placeholder="支持MarkDown"
                            autoSize={{ minRows: 6, maxRows: 10 }}
                        />
                    </div>
                </Modal>
                <Footer></Footer>
            </main>
        </>
    );
}
export default observer(IndexPage)