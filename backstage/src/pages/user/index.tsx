import React, { useEffect, useState } from 'react'
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore'
import styles from './users.less'
import { IValue, IUserItem, IPayload } from '@/types/index'
import Froms from '@/components/users/From';
import { Table, Button, message, Tooltip,ConfigProvider } from 'antd';
import moment from 'moment';
import Footer from '@/components/Footer/Footer';
import BreadHeader from '@/components/Header/BreadHeader';
import { useHistory } from 'umi';
import zhCN from 'antd/es/locale/zh_CN'; 
const user = () => {
    const store = useStore()
    const { userList, total } = store.users
    const [page, setPage] = useState(1)
    const [flag, setFlag] = useState(false)
    const history = useHistory();
    const pathname = history.location.pathname;
    const [titleArr, settitleArr] = useState(['用户管理'])
    const [pageSize, setPageSize] = useState(['8', '12', '24', '36'])
    const [result, setResult] = useState(false)
    function initData(data: IPayload) {
        setFlag(true)
        store.users.getUserList(data).then(res => {
            setFlag(flag)
        })
    }
    //保存选中的数据
    const [arrId, setArrId] = useState([])
    let newArr: IUserItem[] = []
    //生命周期
    useEffect(() => {
        //获取数据
        initData({ page })


    }, [])
    //搜索功能
    const onFinish = (values: IValue) => {
        console.log(values);
        initData({ page, name: values.name, email: values.email, status: values.status, role: values.role })
    };

    //antd 复选框
    const [selectedRowKeys, setSelectedRowKeys] = useState([]);
    const onSelectChange = (selectedRowKeys: React.SetStateAction<never[]>) => {
        console.log('selectedRowKeys changed: ', selectedRowKeys);
        setArrId(selectedRowKeys)

        setSelectedRowKeys(selectedRowKeys);
    };
    //antd 的复选框
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    const hasSelected = selectedRowKeys.length > 0;
    //复选框的启用..多个启用
    const multipleStart = () => {
        //保存选中的数据
        userList.filter(item => {
            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.users.getUserUpdate({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: item.role, status: `active`, updateAt: item.updateAt })
            initData({ page })
        })
        message.success('操作成功')
    }
    //复选框的禁用、、多个禁用
    const multipleBan = () => {
        //保存选中的数据
        userList.filter(item => {
            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.users.getUserUpdate({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: item.role, status: `locked`, updateAt: item.updateAt })
            initData({ page })
        })
        message.success('操作成功')
    }
    //复选框的解除权限、、多个解除
    const multipleLift = () => {
        //保存选中的数据
        userList.filter(item => {
            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.users.getUserUpdate({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: 'visitor', status: item.status, updateAt: item.updateAt })
            initData({ page })
        })
        message.success('操作成功')
    }
    //复选框的授权。。多个授权
    const multipleAuthorization = () => {
        //保存选中的数据
        userList.filter(item => {
            arrId.map((item2) => {
                if (item.id === item2) {
                    return newArr.push(item)
                }
            })
        })
        newArr.forEach(async (item, index) => {
            await store.users.getUserUpdate({ id: item.id, avatar: item.avatar, email: item.email, createAt: item.createAt, name: item.name, role: 'admin', status: item.status, updateAt: item.updateAt })
            initData({ page })
        })
        message.success('操作成功')
    }

    const columns = [
        {
            title: '状态',
            // width: 150,
            dataIndex: 'name',
            key: 'name',
            fontWeight: '900',
            color: '#000',
            fontSize: '20px'
        },
        {
            title: '邮箱',
            dataIndex: 'email',
            key: '1',
            // width: 150
        },
        {
            title: '角色',
            dataIndex: 'role',
            key: '2',
            // width: 80,
            render: (values: string, row: IUserItem, index: number) => {
                return <span>{values === "visitor" ? '访客' : '管理员'}</span>
            }
        },
        {
            title: '状态',
            dataIndex: 'status',
            key: '3',
            // width: 150,
            render: (values: string, row: IUserItem, index: number) => {
                return <div className={styles.user_flag}><span className={values === "active" ? styles.green : styles.orange}></span>{values === "locked" ? '已锁定' : '可用'}</div>
            }
        },
        {
            title: '注册时间',
            dataIndex: 'address',
            key: '4',
            width: 150,
            render: (value: boolean, row: IUserItem, index: number) => {
                return (
                    <div>
                        {moment(row.createAt).format("YYYY-MM-DD HH:mm:ss")}
                    </div>
                )
            }
        },
        {
            title: '操作',
            key: 'operation',
            textAlign: 'center',

            render: (value: string, row: IUserItem, index: number) => {
                return (<div className={styles.user_table_edit}>
                    <a onClick={async () => {
                        await store.users.getUserUpdate({ id: row.id, avatar: row.avatar, email: row.email, createAt: row.createAt, name: row.name, role: row.role, status: `${row.status !== 'locked' ? 'locked' : 'active'}`, updateAt: row.updateAt })
                        initData({ page })
                        message.success('操作成功')
                    }}>{userList[index].status !== "locked" ? '禁用' : '启动'}</a>
                    <a onClick={async () => {
                        await store.users.getUserUpdate({ id: row.id, avatar: row.avatar, email: row.email, createAt: row.createAt, name: row.name, role: `${row.role !== 'visitor' ? 'visitor' : 'admin'}`, status: row.status, updateAt: row.updateAt })
                        initData({ page })
                        message.success('操作成功')
                    }}
                    >{userList[index].role === "visitor" ? '授权' : '解除授权'}</a>
                </div>)
            }
        }
    ];
    return (
        <div className={styles.user} >
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
            <div className={styles.user_box}>
                <div className={styles.user_search}>
                    <Froms onFinish={onFinish} />
                </div>
                <div className={styles.user_table}>
                    <div className={styles.user_table_button_icon}>
                        <Tooltip placement="top" title='刷新'>
                            <i className={'iconfont icon-shuaxin'}></i>
                        </Tooltip>
                    </div>
                    <div className={styles.user_table_box}>
                        {hasSelected ?
                            <ul className={styles.user_table_button}>
                                <li><Button onClick={multipleStart}>启用</Button></li>
                                <li><Button onClick={multipleBan}>禁用</Button></li>
                                <li><Button onClick={multipleLift}>解除授权</Button></li>
                                <li><Button onClick={multipleAuthorization}>授权</Button></li>
                            </ul>
                            : ''}
                    </div>
                    <ConfigProvider locale={zhCN}> 
                    <Table
                        rowKey="id"
                        tableLayout={'fixed'}
                        loading={flag}
                        rowSelection={rowSelection as any}
                        columns={columns as any}
                        dataSource={userList}
                        //   scroll={{ x: 1400}} 
                        pagination={{
                            showSizeChanger: true,
                            defaultPageSize: 12,
                            pageSizeOptions: pageSize,
                            showTotal: (total, range) => {
                                return `共${total}条`
                            },
                        }}
                        style={{ overflow: 'hiddle', fontSize: '22px', display: 'inline-block', color: 'black', fontWeight: 510, }}
                    />
                    </ConfigProvider>
                </div>
            </div>
            <Footer></Footer>
        </div>
    )
}
export default observer(user)