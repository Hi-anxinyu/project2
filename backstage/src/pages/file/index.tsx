import React, { useEffect, useState ,} from 'react'
//引入上传文件
import { Upload, message } from 'antd';
import styles from './file.less'
import UpLoad from '@/components/file/UpLoad'
import Froms from '@/components/file/From'
import { IValuess,IPayloads } from '@/types';
import { observer } from 'mobx-react-lite';
import useStore from '@/context/useStore'
import Item from '@/components/file/Item'
import NoData from '@/components/file/NoData'
import Footer from '@/components/Footer/Footer'
import BreadHeader from '@/components/Header/BreadHeader';
import {useHistory} from 'umi';
const file = () =>{
    const history = useHistory();
    const pathname = history.location.pathname;
    const store = useStore()
    const { fileList,total } = store.file
    const [pageSize, setPageSize] = useState(8)
    const [page,setPage]=useState(1)
    const [titleArr,settitleArr] = useState(['文件管理'])
    function initData(data:IPayloads) {
        store.file.getFileList(data)
    }
    useEffect(() => {
        initData({page,pageSize})
    },[])
    //表格 
    const onFinish = (values: IValuess) => {

        initData({page,originalname:values.originalname,type:values.type})
    };
    //分页
    const changePage=async (current:number,pageSize:number)=>{
        console.log(current,pageSize);
       await setPage(current);
        setTimeout(()=>{
            initData({page:current,pageSize})
        },100)
    }
    //文件上传的东西
    const upload = {
        name: 'file',
        // multiple: true,
        action: '/api/file/upload',
        onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {        
          const { status } = info.file;
          if (status !== 'uploading') {
            console.log(info.file, info.fileList);
            console.log(1);
            
          }
          if (status === 'done') {
            message.success(`${info.file.name} file uploaded successfully.`);
            console.log(2);
          } else if (status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
            console.log(3);

        }
        },
        onDrop(e: { dataTransfer: { files: any; }; }) {
          console.log('Dropped files', e.dataTransfer.files);
          console.log(e.dataTransfer.files);
          
        },
        itemRender(){
            
        },
        showUploadList:true, 
      };
    return (
        <div className={styles.file}>
            <BreadHeader pathnane={pathname} titleArr={titleArr}></BreadHeader>
            <div className={styles.file_content}>
               <UpLoad upload={upload}/>
                <div className={styles.file_form}>
                    <Froms onFinish={onFinish}/>
                </div>
                <div className={styles.file_List}>
                </div>
                <div className={styles.file_Item}>
                    {fileList.length >1?<Item page={page} fileList={fileList} total={total} changePage={changePage}/>:<NoData/>}
                </div>
            </div>
            <Footer/>
        </div>
    )
}
export default observer(file)
