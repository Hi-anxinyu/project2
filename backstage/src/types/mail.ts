export interface ISmtpItem{
    from?:string,
    to?:string,
    subject?:string
}
export interface IValues{
    from:string;
    to:string;
    subject:string;
  }
export interface IMailItem {
  id: string;
  from: string;
  to: string;
  subject: string;
  text: string;
  html: string;
  createAt: string;
}