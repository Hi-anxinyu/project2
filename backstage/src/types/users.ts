export interface IValue{
    name:string;
    email:string;
    role:string;
    status:string;
  }
 export interface IPayload{
    name?: string;
    email?:string;
    status?:string;
    role?:string;
    page:number;
    pageSize?:number;
}
export interface IUserItem {
  id: string;
  name: string;
  avatar?: null|string;
  email?: null| string;
  role: string;
  status: string;
  createAt: string;
  updateAt: string;
}
