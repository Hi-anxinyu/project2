export interface IArticleList {
  id: string;
  title: string;
  cover?: any;
  summary?: any;
  content: string;
  html: string;
  toc: string;
  status: string;
  views: number;
  likes: number;
  isRecommended: boolean;
  needPassword: boolean;
  totalAmount?: any;
  isPay: boolean;
  isCommentable: boolean;
  publishAt: string;
  createAt: string;
  updateAt: string;
  tags: any[];
  category?: any;
}

export interface ICategoryList {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
  articleCount: number;
}

export interface ITags {
  id: string;
  label: string;
  value: string;
  createAt: string;
  updateAt: string;
}
