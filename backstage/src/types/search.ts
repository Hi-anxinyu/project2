export interface ISearch {
  id: string;
  type: string;
  keyword: string;
  count: number;
  createAt: string;
  updateAt: string;
}
export interface IKnowledgeSearch {
  page: number
  pageSize: number
  title: string
  status: string
}
export interface IFileSearch {
  originalname: string,
  type: string
}
export interface ISearchList {
  type?: string
  keyword?: string
  count?: string
}