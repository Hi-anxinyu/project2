export interface ICommentItem {
  comment: any;
  id: string;
  name: string;
  email: string;
  content: string;
  html: string;
  pass: boolean|string;
  userAgent: string;
  hostId: string;
  url: string;
  parentCommentId?: string;
  replyUserName?: string;
  replyUserEmail?: string;
  createAt: string;
  updateAt: string;
}
export interface Payload{
  pageSize?:number;
  page:number;
  name?:string;
  email?:string;
  pass?:number;
}
export interface Values{
  name:string;
  email:string;
  pass:number;
}
export interface IAction{
  id:string,
  pass:boolean, 
}
export interface IPayloadTo {
  name: string;
  email: string;
  content: string;
  parentCommentId: string;
  hostId: string;
  replyUserName: string;
  replyUserEmail: string;
  url: string;
  createByAdmin: boolean;
}