import Cookies from 'js-cookie';
const key = 'authorization';

//设置登录态
export function setToken(value:string){
    let now = +new Date()
    let expires = new Date(now+24*60*60*1000);
    Cookies.set(key,value,{expires})
}
//获取登录态
export function getToken(){
    return'Bearer '+Cookies.get(key);
}
//移除登录态
export function removeToken(){
    Cookies.remove(key);
}