const showdown = require('showdown');
const converter = new showdown.Converter();
const { Sitdown } = require('sitdown')
// markdown转为HTML
export function makeHtml(md: string) {
  return converter.makeHtml(md);
}

// 通过markdown的标题生成toc
export function makeToc(html: string) {
  const reg = /<h([/d]) id="([^<]+)">([^<]+)<\h([\d])/gi;
  let ret = null;
  const toc = [];
  while ((ret = reg.exec(html)) !== null) {
    toc.push({level: ret[1], id: ret[2], text: ret[3]});
  }
  return toc;
}
