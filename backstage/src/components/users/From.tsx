import React from 'react'

import {  Form, Input, Select, Row, Col,Button} from 'antd';
import { IValue } from '@/types';
interface Props{
    onFinish(values:IValue):void
}
 const Froms:React.FC<Props>= (props) => {
    const [form] = Form.useForm();
    const {onFinish}=props  
    //哈哈
    const onReset = () => {
        form.resetFields();
    };
    return (
        <div>
             <Form style={{marginLeft:'30px'}} form={form} name="control-hooks" layout="inline" onFinish={onFinish}>
                    <Row gutter={24}>
                    <Form.Item  
                        name='name'
                        label="账户"
                        rules={[{ message: 'Please input your username!' }]}
                    >
                        <Input placeholder='请输入账户'/>
                    </Form.Item>
                    <Form.Item
                        name='email'
                        label="Email"   
                    >
                        <Input placeholder="请输入账户邮箱"/>
                    </Form.Item>
                    <Form.Item label="角色" name="role" style={{width:'135px'}}>
                        <Select>
                            <Select.Option value={'admin'}>管理员</Select.Option>
                            <Select.Option value={'visitor'}>访客</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label="状态" name="status" style={{width:'135px'}}>
                        <Select>
                            <Select.Option value={'locked'}>锁定</Select.Option>
                            <Select.Option value={'active'}>可用</Select.Option>
                        </Select>
                    </Form.Item>
                    </Row>
                
                    <Col span={24} style={{ textAlign: 'right' }}>
                    <Form.Item>
                        <Button type="primary" htmlType="submit" >
                            搜索
                        </Button>
                        <Button htmlType="button" onClick={onReset} style={{ margin: '0 8px' }}>
                            重置
                        </Button>
                    </Form.Item>
                    </Col>
                </Form>
        </div>
    )
}
export default Froms