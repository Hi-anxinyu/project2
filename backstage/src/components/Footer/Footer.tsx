import React from 'react'
import styles from './index.less';
import { GithubOutlined, CopyrightOutlined } from '@ant-design/icons';
function Footer() {
    return (
        <footer>
            <ul className={styles.footlist}>
                <li>
                    <a href="">
                        <GithubOutlined style={{ color: '#333' }} />
                    </a>
                </li>
            </ul>
            <div>
                <p>
                    <span>Copyright</span>
                    <CopyrightOutlined style={{ fontSize: 12 }} />
                    <span>2021</span>
                    <span>Designed by</span>
                    <a href="" style={{ color: '#333' }}>
                        <span>Fantasticit.</span>
                    </a>
                </p>
            </div>
        </footer>
    )
}
export default Footer
