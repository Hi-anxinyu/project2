import React, { useState } from 'react'
import {  Form, Input, Select, Row, Col,Button} from 'antd';
import {IValuess} from '@/types'
const Froms:React.FC =(props)=> {
    const [form] = Form.useForm();
    const onReset = () => {
          form.resetFields();
    };
    const onFinish = (values:{name:string}) => {
    };
    return (
        <Form 
                    style={{backgroundColor: '#fff',padding:25}} 
                    layout="inline"
                    onFinish={onFinish}
                    form={form}
                    >
                        <Row>
                            <Form.Item
                                name='name'
                                label="文件名"
                                rules={[{ message: 'Please input your username!' }]}
                            >
                                <Input placeholder='请输入文件名称' />
                            </Form.Item>
                        </Row>
                        <Col span={24} style={{ textAlign: 'right' }}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{marginRight:12}}>
                                    搜索
                                </Button>
                                <Button htmlType="reset" onReset={onReset}>
                                    重置
                                </Button>
                            </Form.Item>
                        </Col>
                    </Form>
    )
}
export default Froms