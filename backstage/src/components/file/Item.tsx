import React, { useState } from 'react'
import { IFileItem } from '@/types'
import styles from '@/pages/file/file.less'
import moment from 'moment'
import { Pagination,ConfigProvider } from 'antd';
import Drawer from './Drawer'
import zhCN from 'antd/es/locale/zh_CN'; 
interface Props{
    fileList:IFileItem[]
    total:number
    changePage(current:number,pageSize:number):void
    page:number
}
 const Item:React.FC<Props>=(props)=>{
     const [item,setItem]=useState({})
     //抽屉
     const {fileList,total,changePage,page} = props
     const [visible, setVisible] = useState(false);
     const showDrawer =async (item:IFileItem) => {
      await setVisible(true);
      await setItem(item)
     };
     const onClose = () => {
        setVisible(false);
     };
    return (
        <>  
        <ul className={styles.file_item_list}>
            {
                fileList.map(item=>{
                    return <li key={item.id} onClick={showDrawer.bind(this,item as IFileItem) as unknown as React.MouseEventHandler<HTMLLIElement>}>
                              <div className={styles.img}><img src={item.url} alt="" /></div>
                              <div className={styles.file_item_text}>
                                  <span>{item.originalname}</span>
                                  <span>上传时间为{moment(item.createAt).format('YYYY-MM-DD HH:mm:ss')}</span>
                              </div>
                           </li>
                })
            }
        </ul>
        <div className={styles.file_page}>
        <ConfigProvider locale={zhCN}>
        <Pagination total={total}
             showSizeChanger={true}
             defaultCurrent={1}
             current={page}
             defaultPageSize={8}
             pageSizeOptions={['8','12','24','36']}                    
             showTotal={(total:number)=>{
               return `共${total}条`
             }}
             hideOnSinglePage={true}
             onChange={changePage as any}
             
        />
        </ConfigProvider>
        </div>
        <Drawer visible={visible} item={item} onClose={onClose}/>
        </>
    )
}
export default Item