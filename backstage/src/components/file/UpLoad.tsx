import React from 'react'
import { Upload, message } from 'antd';
import styles from '../../pages/file/file.less'
import { InboxOutlined } from '@ant-design/icons';
const { Dragger } = Upload;
 interface UpLoad{
    upload: {
        name: string,
        action: string,
        onChange(info: { file: { name?: any; status?: any; }; fileList: any; }):void
        onDrop(e: { dataTransfer: { files: any; }; }):void
        // itemRender():any
    }
 }
 //上传文件组件
 const UpLoad:React.FC<UpLoad>=(props)=> {
     const {upload}=props
    return (
        <div>
             <div className={styles.file_upload}>
                    <Dragger {...upload}>
                        <p className="ant-upload-drag-icon">
                        <InboxOutlined />
                        </p>
                        <p className="ant-upload-text">点击选择文件或者将文件拖拽到此处</p>
                        <p className="ant-upload-hint">
                        文件上传到OSS,如未配置清先配置
                        </p>
                    </Dragger>
                </div>
        </div>
    )
}
export default UpLoad