import React from 'react'
import { Empty } from 'antd';
import styles from '@/pages/file/file.less'
//没有数据是显示的组件
export default function NoData() {
    return (
        <div className={styles.file_Item_to}>
           <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        </div>
    )
}
