import React, { useState } from 'react'
import {  Form, Input, Select, Row, Col,Button} from 'antd';
import {IValuess} from '@/types'
interface Props{
    onFinish(values:IValuess):void
}
 const Froms:React.FC<Props> =(props)=> {
    const [form] = Form.useForm();
    const {onFinish}=props  
    const onReset = () => {
          form.resetFields();
      };
    return (
        <Form style={{marginLeft:'30px'}} form={form} name="control-hooks" layout="inline" onFinish={onFinish}>
            <Row gutter={24}>
            <Form.Item  
                name='originalname'
                label="文件名"
                rules={[{ message: 'Please input your username!' }]}
            >
                <Input placeholder='请输入文件名称'/>
            </Form.Item>
            <Form.Item
                name='type'
                label="文件类型"   
            >
                <Input placeholder="请输入文件类型"/>
            </Form.Item>
            </Row>
         
            <Col span={24} style={{ textAlign: 'right' }}>
            <Form.Item>
                <Button type="primary" htmlType="submit" >
                     搜索
                </Button>
                <Button htmlType="button" onClick={onReset} style={{ margin: '0 8px' }}>
                    重置
                </Button>
            </Form.Item>
            </Col>
        </Form>
    )
}
export default Froms