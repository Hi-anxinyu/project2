import React, { useState } from 'react';
import { Drawer, Button,message } from 'antd';
import styles from '@/pages/file/file.less'
import { IFileItem } from '@/types';
import copy from 'copy-to-clipboard';
import useStore from '@/context/useStore';
// var  copy  = require ( 'copy' ) ; 
interface Props{
    onClose?():void
    visible?:boolean
    item?:any
}
const Drawers: React.FC<Props> = (props) => {
    const {onClose,visible,item} = props;
    console.log(item);
    const store = useStore()
    const { fileList,total } = store.file
    function Copy(){
        copy(item.url);
        message.success('内容已复制到剪切板')
    }
    async function onRemove (){
      await store.file.setDelete(item.id)      
    }
    return (
    <>
      <Drawer width={640} title="文件信息" placement="right" onClose={onClose} visible={visible}>
        <ul className={styles.file_details}>
            <li><img src={item!.url} alt="" /></li>
            <li>文件名称: <span>{item.originalname}</span></li>
            <li>存储路径: <span>{item.filename}</span></li>
            <li className={styles.File_details_item3}><span>文件类型: {item.type}</span><span>文件大小: {(item.size / 1024).toFixed(2) + "KB"}</span></li>
            <li>访问链接: <span onClick={Copy}>{item.url}</span></li>
            <li><span onClick={Copy}>复制</span></li>
        </ul>
        <div className={styles.File_details_remove}>
          <Button onClick={onClose}>关闭</Button>
          <Button danger onClick={onRemove}>删除</Button>
        </div>
     </Drawer>
    </>
  );
};

export default Drawers
