{/* 渲染的数据 */ }
import { message, Tooltip, Popconfirm, Form, Input, Button, Drawer, Switch, Upload, Row, Col, Pagination, Card } from 'antd';
import {
    InboxOutlined,
} from '@ant-design/icons'
import { IKnowledge } from "@/types"
import { FC, useCallback, useEffect, useState } from "react"
import useStore from '@/context/useStore';
import { NavLink } from "umi"
import ImageView from '../ImageView';
import styles from "@/pages/knowledge/index.less"


interface Props {
    knowledgeList: IKnowledge[]
}
const Draw: FC<Props> = (props) => {
    //从父组件传过来的数据
    let {
        knowledgeList
    } = props
    const [form] = Form.useForm();
    const { Meta } = Card;
    const { Dragger } = Upload;
    const store = useStore();
    const [page, setpage] = useState(1);
    const [pageSize, setpageSize] = useState(12);
    const [params, setParams] = useState({});
    const [visible, setVisible] = useState(false);
    const [childrenDrawer, setchildrenDrawer] = useState(false);
    const [id, setId] = useState("");
    //打开抽屉
    async function changeB(id: string) {
        setVisible(true);
        await store.knowledge.getFileList(page, pageSize);
        // console.log(id);
        setId(id);
        knowledgeList.forEach(item => {
            if (item.id === id) {
                // console.log(item);
                form.setFieldsValue(item);
            }
        })
    }
    //关闭弹框
    function onClose() {
        setVisible(false);
    }
    //打开二级弹框
    function showChildrennDrawer() {
        setchildrenDrawer(true);
    }
    //关闭二级弹框
    function onChildrenDrawerClose() {
        setchildrenDrawer(false);
    }
    //发布草稿的状态改变
    async function change(id: string, status: string) {
        if (status === "publish") {
            await store.knowledge.changeKonwledgeList(id, status = "draft");
        } else {
            await store.knowledge.changeKonwledgeList(id, status = "publish");
        }
        // await store.knowledge.changeKonwledgeList(id, status = "publish");
        await store.knowledge.getKonwledgeList(page, pageSize, params)
    }
    //更新按钮
    async function SetUp() {
        let values = form.getFieldsValue();
        console.log(values, "=======values=====");
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
        // console.log(params)
        await store.knowledge.setKnowledgeList(id, params);
        // setVisible(false);
        await store.knowledge.getKonwledgeList(page, pageSize, params)
    }
    //开关
    function onChange(checked: any) {
        console.log(`switch to ${checked}`);
    }
    //二层抽屉的form表单
    async function onFinish() {
        let values = form.getFieldsValue();
        console.log(values, "=======values=====");
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
        await store.knowledge.getFileList(page, pageSize, params);
    };
    //移除按钮
    function remove() {

    }
    //数据反显
    const submit = useCallback(
        (value: any) => {
            console.log(value);
        }, [],
    )
    //删除的确定按钮
    async function confirm(id: string) {
        await store.knowledge.deleteKonwledgeList(id);
        await store.knowledge.getKonwledgeList(page, pageSize, params)
    }
    //删除的取消框
    function cancel() {
        message.error('Click on No');
    }
    return (
        <div className={styles.box0}>
            {
                knowledgeList.map(item => {
                    return <div className={styles.box1} key={item.id}>
                        <img src={item.cover} alt="" />
                        <div className={styles.box2}>
                            <div>{item.title}</div>
                            <div>{item.summary}</div>
                        </div>
                        <ul className={styles.ul1}>
                            <li>
                                <span>
                                    <NavLink to={`/knowledge/editor/${item.id}`}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="edit" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M257.7 752c2 0 4-.2 6-.5L431.9 722c2-.4 3.9-1.3 5.3-2.8l423.9-423.9a9.96 9.96 0 000-14.1L694.9 114.9c-1.9-1.9-4.4-2.9-7.1-2.9s-5.2 1-7.1 2.9L256.8 538.8c-1.5 1.5-2.4 3.3-2.8 5.3l-29.5 168.2a33.5 33.5 0 009.4 29.8c6.6 6.4 14.9 9.9 23.8 9.9zm67.4-174.4L687.8 215l73.3 73.3-362.7 362.6-88.9 15.7 15.6-89zM880 836H144c-17.7 0-32 14.3-32 32v36c0 4.4 3.6 8 8 8h784c4.4 0 8-3.6 8-8v-36c0-17.7-14.3-32-32-32z"></path></svg>
                                    </NavLink>
                                </span>
                            </li>
                            <li>
                                <span onClick={() => {
                                    change(item.id, item.status)
                                }}>
                                    <Tooltip title={item.status === "publish" ? "设为草稿" : "发布上线"} >
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="cloud-upload" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M518.3 459a8 8 0 00-12.6 0l-112 141.7a7.98 7.98 0 006.3 12.9h73.9V856c0 4.4 3.6 8 8 8h60c4.4 0 8-3.6 8-8V613.7H624c6.7 0 10.4-7.7 6.3-12.9L518.3 459z"></path><path d="M811.4 366.7C765.6 245.9 648.9 160 512.2 160S258.8 245.8 213 366.6C127.3 389.1 64 467.2 64 560c0 110.5 89.5 200 199.9 200H304c4.4 0 8-3.6 8-8v-60c0-4.4-3.6-8-8-8h-40.1c-33.7 0-65.4-13.4-89-37.7-23.5-24.2-36-56.8-34.9-90.6.9-26.4 9.9-51.2 26.2-72.1 16.7-21.3 40.1-36.8 66.1-43.7l37.9-9.9 13.9-36.6c8.6-22.8 20.6-44.1 35.7-63.4a245.6 245.6 0 0152.4-49.9c41.1-28.9 89.5-44.2 140-44.2s98.9 15.3 140 44.2c19.9 14 37.5 30.8 52.4 49.9 15.1 19.3 27.1 40.7 35.7 63.4l13.8 36.5 37.8 10C846.1 454.5 884 503.8 884 560c0 33.1-12.9 64.3-36.3 87.7a123.07 123.07 0 01-87.6 36.3H720c-4.4 0-8 3.6-8 8v60c0 4.4 3.6 8 8 8h40.1C870.5 760 960 670.5 960 560c0-92.7-63.1-170.7-148.6-193.3z"></path></svg>
                                    </Tooltip >
                                </span>
                            </li>
                            <li>
                                <span>
                                    <span onClick={() => {
                                        changeB(item.id);
                                    }}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="setting" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M924.8 625.7l-65.5-56c3.1-19 4.7-38.4 4.7-57.8s-1.6-38.8-4.7-57.8l65.5-56a32.03 32.03 0 009.3-35.2l-.9-2.6a443.74 443.74 0 00-79.7-137.9l-1.8-2.1a32.12 32.12 0 00-35.1-9.5l-81.3 28.9c-30-24.6-63.5-44-99.7-57.6l-15.7-85a32.05 32.05 0 00-25.8-25.7l-2.7-.5c-52.1-9.4-106.9-9.4-159 0l-2.7.5a32.05 32.05 0 00-25.8 25.7l-15.8 85.4a351.86 351.86 0 00-99 57.4l-81.9-29.1a32 32 0 00-35.1 9.5l-1.8 2.1a446.02 446.02 0 00-79.7 137.9l-.9 2.6c-4.5 12.5-.8 26.5 9.3 35.2l66.3 56.6c-3.1 18.8-4.6 38-4.6 57.1 0 19.2 1.5 38.4 4.6 57.1L99 625.5a32.03 32.03 0 00-9.3 35.2l.9 2.6c18.1 50.4 44.9 96.9 79.7 137.9l1.8 2.1a32.12 32.12 0 0035.1 9.5l81.9-29.1c29.8 24.5 63.1 43.9 99 57.4l15.8 85.4a32.05 32.05 0 0025.8 25.7l2.7.5a449.4 449.4 0 00159 0l2.7-.5a32.05 32.05 0 0025.8-25.7l15.7-85a350 350 0 0099.7-57.6l81.3 28.9a32 32 0 0035.1-9.5l1.8-2.1c34.8-41.1 61.6-87.5 79.7-137.9l.9-2.6c4.5-12.3.8-26.3-9.3-35zM788.3 465.9c2.5 15.1 3.8 30.6 3.8 46.1s-1.3 31-3.8 46.1l-6.6 40.1 74.7 63.9a370.03 370.03 0 01-42.6 73.6L721 702.8l-31.4 25.8c-23.9 19.6-50.5 35-79.3 45.8l-38.1 14.3-17.9 97a377.5 377.5 0 01-85 0l-17.9-97.2-37.8-14.5c-28.5-10.8-55-26.2-78.7-45.7l-31.4-25.9-93.4 33.2c-17-22.9-31.2-47.6-42.6-73.6l75.5-64.5-6.5-40c-2.4-14.9-3.7-30.3-3.7-45.5 0-15.3 1.2-30.6 3.7-45.5l6.5-40-75.5-64.5c11.3-26.1 25.6-50.7 42.6-73.6l93.4 33.2 31.4-25.9c23.7-19.5 50.2-34.9 78.7-45.7l37.9-14.3 17.9-97.2c28.1-3.2 56.8-3.2 85 0l17.9 97 38.1 14.3c28.7 10.8 55.4 26.2 79.3 45.8l31.4 25.8 92.8-32.9c17 22.9 31.2 47.6 42.6 73.6L781.8 426l6.5 39.9zM512 326c-97.2 0-176 78.8-176 176s78.8 176 176 176 176-78.8 176-176-78.8-176-176-176zm79.2 255.2A111.6 111.6 0 01512 614c-29.9 0-58-11.7-79.2-32.8A111.6 111.6 0 01400 502c0-29.9 11.7-58 32.8-79.2C454 401.6 482.1 390 512 390c29.9 0 58 11.6 79.2 32.8A111.6 111.6 0 01624 502c0 29.9-11.7 58-32.8 79.2z"></path></svg>
                                    </span>
                                </span>
                            </li>
                            <li>
                                <span>
                                    <Popconfirm
                                        title="确定删除?"
                                        onConfirm={() => {
                                            confirm(item.id)
                                        }}
                                        onCancel={cancel}
                                        okText="确认"
                                        cancelText="取消"
                                    >
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="delete" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M360 184h-8c4.4 0 8-3.6 8-8v8h304v-8c0 4.4 3.6 8 8 8h-8v72h72v-80c0-35.3-28.7-64-64-64H352c-35.3 0-64 28.7-64 64v80h72v-72zm504 72H160c-17.7 0-32 14.3-32 32v32c0 4.4 3.6 8 8 8h60.4l24.7 523c1.6 34.1 29.8 61 63.9 61h454c34.2 0 62.3-26.8 63.9-61l24.7-523H888c4.4 0 8-3.6 8-8v-32c0-17.7-14.3-32-32-32zM731.3 840H292.7l-24.2-512h487l-24.2 512z"></path></svg>
                                    </Popconfirm>
                                </span>
                            </li>
                        </ul>
                    </div>
                })
            }
            {/* 一层抽屉 */}
            <Drawer
                title="更新知识库"
                width={500}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button onClick={onClose} style={{ marginRight: 8 }}>
                            取消
                        </Button>
                        <Button type="primary" onClick={SetUp}>
                            更新
                        </Button>
                    </div>
                }
            >
                <Form
                    form={form}
                    onFinish={submit}
                >
                    <Form.Item
                        name="title"
                        label="名称"
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="summary"
                        label="描述"
                    >
                        <Input.TextArea rows={4} />
                    </Form.Item>
                    <Form.Item
                        name="isCommentable"
                        label="评论"
                    >
                        <Switch onChange={onChange} />
                    </Form.Item>
                    <Form.Item
                        name="cover"
                        label="封面"
                    >
                        <Dragger {...props} >
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                            <p className="ant-upload-hint">
                                文件将上传到OSS，如未配置请先配置
                            </p>
                        </Dragger>
                    </Form.Item>
                    <Form.Item>
                        <Input />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={showChildrennDrawer}>选择文件</Button>
                        <Button danger onClick={remove} style={{ marginLeft: "8px" }}>移除</Button>
                        {/* 二层抽屉 */}
                        <Drawer
                            title="文件选择"
                            width={800}
                            closable={true}
                            onClose={onChildrenDrawerClose}
                            visible={childrenDrawer}
                        >
                            <Form
                                form={form}
                                name="advanced_search"
                                className="ant-advanced-search-form"
                                onFinish={onFinish}
                            >
                                <div>
                                    <Row gutter={24}>
                                        <Col span={8}>
                                            <Form.Item
                                                name="originalname"
                                                label="文件名"
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Input something!',
                                                    },
                                                ]}
                                            >
                                                <Input placeholder="请输入文件名" />
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={24}>
                                        <Col span={8}>
                                            <Form.Item
                                                name="type"
                                                label="文件类型"
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Input something!',
                                                    },
                                                ]}
                                            >
                                                <Input placeholder="请输入文件类型" />
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </div>
                                <Row>
                                    <Col span={24} style={{ textAlign: 'right' }}>
                                        <Button type="primary" htmlType="submit">
                                            搜索
                                        </Button>
                                        <Button
                                            style={{ margin: '0 8px' }}
                                            onClick={() => {
                                                form.resetFields();
                                            }}
                                        >
                                            重置
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                            <div className={styles.box643}>
                                <div className={styles._4yxeklcCZVaddgSjYswZG}>
                                    <Upload {...props}>
                                        <Button>文件上传</Button>
                                    </Upload>
                                </div>
                                <div>
                                    {
                                        store.knowledge.fileList && store.knowledge.fileList.map(item => {
                                            return <div key={item.id} style={{ width: "25%", float: "left" }}>
                                                <Card
                                                    hoverable
                                                    style={{ width: 165.1 }}
                                                    cover={<ImageView><img alt="example" src={item.url} className={styles.img} /></ImageView>}
                                                >
                                                    <Meta title={item.originalname} />
                                                </Card>
                                            </div>
                                        })
                                    }
                                </div>
                                <div className={styles.Jk_7c4uLfV5bARw2xisN}>
                                    <Pagination
                                        total={store.knowledge.fileCount}
                                        showTotal={total => `共 ${store.knowledge.fileCount} 条`}
                                        defaultPageSize={12}
                                        defaultCurrent={1}
                                        showSizeChanger={true}
                                        pageSizeOptions={["8", "12", "24", "36"]}
                                    />
                                </div>
                            </div>
                        </Drawer>
                    </Form.Item>
                </Form>
            </Drawer>
        </div>
    )
}
export default Draw
