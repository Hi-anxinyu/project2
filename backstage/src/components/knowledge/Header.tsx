import { NavLink } from "umi"
import styles from "@/pages/knowledge/index.less"
import { Breadcrumb } from "antd"


const Header = () => {
    return (
        <header>
             <Breadcrumb>
                <Breadcrumb.Item>
                    <NavLink to="/">工作台</NavLink>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <NavLink to="/knowledge">知识小册</NavLink>
                </Breadcrumb.Item>
            </Breadcrumb>
        </header>
    )
}
export default Header