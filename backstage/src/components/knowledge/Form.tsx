{/* input框和select框和按钮 */ }
import { Form, Row, Col, Input, Button, Select } from 'antd';
import { useState } from 'react';
import useStore from '@/context/useStore';
import styles from "@/pages/knowledge/index.less"

const Form1 = (props: any) => {
    const [form] = Form.useForm();
    const { Option } = Select;
    //传的参数
    const [page, setpage] = useState(1);
    const [pageSize, setpageSize] = useState(12);
    const [params, setParams] = useState({});
    const store = useStore();
    //表单提交事件
    const onFinish = async () => {
        let values = form.getFieldsValue();
        console.log(values, "=======values=====");
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
        await store.knowledge.getKonwledgeList(page, pageSize, params)
    };
    function handleChange(value: any) {
        console.log(`selected ${value}`);
    }
    return (
        <div>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
            >
                <div>
                    <Row gutter={24}>
                        <Col span={8}>
                            <Form.Item
                                name="title"
                                label="名称"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Input something!',
                                    },
                                ]}
                            >
                                <Input placeholder="请输入知识库名称" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={8}>
                            <Form.Item
                                name="status"
                                label="状态"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Input something!',
                                    },
                                ]}
                            >
                                <Select style={{ width: 120 }} onChange={handleChange}>
                                    <Option value="publish">已发布</Option>
                                    <Option value="draft">草稿</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                </div>
                <Row>
                    <Col span={24} style={{ textAlign: 'right' }}>
                        <Button type="primary" htmlType="submit">
                            搜索
                        </Button>
                        <Button
                            style={{ margin: '0 8px' }}
                            onClick={() => {
                                form.resetFields();
                            }}
                        >
                            重置
                        </Button>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}
export default Form1