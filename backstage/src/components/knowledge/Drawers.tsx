{/* 抽屉 */ }
import { Form, Input, Button, Drawer, Switch, message, Upload, Row, Col, Pagination, Card } from 'antd';
import { useState } from 'react';
import {
    InboxOutlined,
    PlusOutlined,
} from '@ant-design/icons'
import useStore from '@/context/useStore';
import { observer } from "mobx-react-lite"
import ImageView from '../ImageView';
import styles from "@/pages/knowledge/index.less"



const Drawers = () => {
    const [form] = Form.useForm();
    const { Dragger } = Upload;
    const { Meta } = Card;
    const store = useStore();
    const [page, setpage] = useState(1);
    const [pageSize, setpageSize] = useState(12);
    //改变弹框的状态
    const [visible, setVisible] = useState(false);
    const [childrenDrawer, setchildrenDrawer] = useState(false);
    const [params, setParams] = useState({});
    const props = {
        name: 'file',
        multiple: true,
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        onChange(info: { file: { name?: any; status?: any; }; fileList: any; }) {
            const { status } = info.file;
            if (status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (status === 'done') {
                message.success(`${info.file.name} file uploaded successfully.`);
            } else if (status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
        onDrop(e: { dataTransfer: { files: any; }; }) {
            console.log('Dropped files', e.dataTransfer.files);
        },
    };
    //打开弹框
    async function showDrawer() {
        setVisible(true);
        await store.knowledge.getFileList(page, pageSize);
    }
    //关闭弹框
    function onClose() {
        setVisible(false);
    }
    //开关
    function onChange(checked: any) {
        console.log(`switch to ${checked}`);
    }
    //打开二级弹框
    function showChildrennDrawer() {
        setchildrenDrawer(true);
    }
    //关闭二级弹框
    function onChildrenDrawerClose() {
        setchildrenDrawer(false);
    }
    //添加数据
    //no
    async function SetUp() {
        let values = form.getFieldsValue();
        console.log(values, "=======values=====");
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
        setVisible(false);
        await store.knowledge.addKonwledgeList(params);
        await store.knowledge.getKonwledgeList(page, pageSize, params)
    }
    //二层抽屉的form表单
    async function onFinish() {
        let values = form.getFieldsValue();
        console.log(values, "=======values=====");
        let params: { [key: string]: string } = {}
        for (let key in values) {
            values[key] && (params[key] = values[key])
        }
        setParams(params)
        await store.knowledge.getFileList(page, pageSize, params);
    };
    return (
        <div>
            <div className={styles.box789}>
                <Button type="primary" icon={<PlusOutlined />} onClick={showDrawer}>
                    新建
                </Button>
            </div>
            {/* 一层抽屉 */}
            <Drawer
                title="新建知识库"
                width={500}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                footer={
                    <div
                        style={{
                            textAlign: 'right',
                        }}
                    >
                        <Button onClick={onClose} style={{ marginRight: 8 }}>
                            取消
                        </Button>
                        <Button type="primary" onClick={SetUp}>
                            创建
                        </Button>
                    </div>
                }
            >
                <Form
                    form={form}
                >
                    <Form.Item
                        name="title"
                        label="名称"
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="summary"
                        label="描述"
                    >
                        <Input.TextArea rows={4} />
                    </Form.Item>
                    <Form.Item
                        name="isCommentable"
                        label="评论"
                    >
                        <Switch onChange={onChange} />
                    </Form.Item>
                    <Form.Item
                        name="cover"
                        label="封面"
                    >
                        <Dragger {...props} >
                            <p className="ant-upload-drag-icon">
                                <InboxOutlined />
                            </p>
                            <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
                            <p className="ant-upload-hint">
                                文件将上传到OSS，如未配置请先配置
                            </p>
                        </Dragger>
                    </Form.Item>
                    <Form.Item>
                        <Input />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={showChildrennDrawer}>选择文件</Button>
                        {/* 二层抽屉 */}
                        <Drawer
                            title="文件选择"
                            width={800}
                            closable={true}
                            onClose={onChildrenDrawerClose}
                            visible={childrenDrawer}
                        >
                            <Form
                                form={form}
                                name="advanced_search"
                                className="ant-advanced-search-form"
                                onFinish={onFinish}
                            >
                                <div>
                                    <Row gutter={24}>
                                        <Col span={8}>
                                            <Form.Item
                                                name="originalname"
                                                label="文件名"
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Input something!',
                                                    },
                                                ]}
                                            >
                                                <Input placeholder="请输入文件名" />
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                    <Row gutter={24}>
                                        <Col span={8}>
                                            <Form.Item
                                                name="type"
                                                label="文件类型"
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Input something!',
                                                    },
                                                ]}
                                            >
                                                <Input placeholder="请输入文件类型" />
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </div>
                                <Row>
                                    <Col span={24} style={{ textAlign: 'right' }}>
                                        <Button type="primary" htmlType="submit">
                                            搜索
                                        </Button>
                                        <Button
                                            style={{ margin: '0 8px' }}
                                            onClick={() => {
                                                form.resetFields();
                                            }}
                                        >
                                            重置
                                        </Button>
                                    </Col>
                                </Row>
                            </Form>
                            <div className={styles.box643}>
                                <div className={styles._4yxeklcCZVaddgSjYswZG}>
                                    <Upload {...props}>
                                        <Button>文件上传</Button>
                                    </Upload>
                                </div>
                                <div>
                                    {
                                        store.knowledge.fileList && store.knowledge.fileList.map(item => {
                                            return <div key={item.id} style={{ width: "25%", float: "left" }}>
                                                <Card
                                                    hoverable
                                                    style={{ width: 165.1 }}
                                                    cover={<ImageView><img alt="example" src={item.url} className={styles.img} /></ImageView>}
                                                >
                                                    <Meta title={item.originalname} />
                                                </Card>
                                            </div>
                                        })
                                    }
                                </div>
                                <div className={styles.Jk_7c4uLfV5bARw2xisN}>
                                    <Pagination
                                        total={store.knowledge.fileCount}
                                        showTotal={total => `共 ${store.knowledge.fileCount} 条`}
                                        defaultPageSize={12}
                                        defaultCurrent={1}
                                        showSizeChanger={true}
                                        pageSizeOptions={["8", "12", "24", "36"]}
                                    />
                                </div>
                            </div>
                        </Drawer>
                    </Form.Item>
                </Form>
            </Drawer>
        </div>
    )
}
export default observer(Drawers);