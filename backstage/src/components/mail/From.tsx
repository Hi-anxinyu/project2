import React from 'react'
import {  Form, Input, Select, Row, Col,Button} from 'antd';
import { IValues } from '@/types';
interface IProps{
    onFinish(values:IValues):void;
}
const Froms:React.FC<IProps> =(props) => {
    const [form] = Form.useForm();
    const {onFinish}=props
    const onReset = () => {
        form.resetFields();
    };
    return (
        <>
        <Form style={{marginLeft:'30px'}} form={form} name="control-hooks" layout="inline" onFinish={onFinish}>
            <Row gutter={24}>
            <Form.Item  
                name='from'
                label="发件人"
                rules={[{ message: 'Please input your username!' }]}
            >
                <Input placeholder='请输入发件人'/>
            </Form.Item>
            <Form.Item
                name='to'
                label="收件人"   
            >
                <Input placeholder="请输入收件人"/>
            </Form.Item>
            <Form.Item
                name='subject'
                label="主题"   
            >
                <Input placeholder="请输入主题"/>
            </Form.Item>
            </Row>
        
            <Col span={24} style={{ textAlign: 'right' }}>
            <Form.Item>
                <Button type="primary" htmlType="submit" >
                    搜索
                </Button>
                <Button htmlType="button" onClick={onReset} style={{ margin: '0 8px' }}>
                    重置
                </Button>
            </Form.Item>
            </Col>
        </Form>
        </>
    )
}
export default Froms