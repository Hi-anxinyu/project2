import styles from './index.less';
import { Breadcrumb, Affix } from 'antd';
import { NavLink } from 'umi';
interface IProps{
    pathnane:string,
    titleArr:string[]
}
const BreadHeader:React.FC<IProps> = ({pathnane,titleArr}) => {
    let str = '/';
    let arr = [{path:'/',title:'工作台'}];
    let pathArr = pathnane.split('/');
    for (let i = 1; i < pathArr.length; i++) {
        str += pathArr[i];
        let obj = {path:str,title:titleArr[i-1]}
        arr.push(obj);
        str+='/'
    }
    return (
        <Affix offsetTop={64.1}>
            <header className={styles.breadheader}>
                <Breadcrumb>
                    {
                        arr.map((item, index) => {
                            return <Breadcrumb.Item key={index}><NavLink to={item.path}>{item.title}</NavLink></Breadcrumb.Item>
                        })
                    }
                </Breadcrumb>
            </header>
        </Affix>
    )
}

export default BreadHeader