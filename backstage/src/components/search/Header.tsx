import React from 'react'
import { Breadcrumb } from "antd"
import { NavLink } from 'umi'

const Header = () => {
    return (
        <header>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <NavLink to="/">工作台</NavLink>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <NavLink to="/search">搜索记录</NavLink>
                </Breadcrumb.Item>
            </Breadcrumb>
        </header>
    )
}
export default Header