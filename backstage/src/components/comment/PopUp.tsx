import React, { useState } from 'react'
import { Modal,Input} from 'antd';
import { ICommentItem } from '@/types';
interface IProps{
    showModal(row:ICommentItem):void,
    hideModal():void,
    val:string,
    visible:boolean,
    item:ICommentItem
    ChangeVal({ target: { value }}:any):void
    clickAffirm():void
}

const PopUp:React.FC<IProps>=(props)=> {
       //回复的弹出框
    const { TextArea } = Input;
    const {visible,showModal,val,hideModal,ChangeVal,clickAffirm} = props;
  
    return (
        <>
        <Modal
            title="回复评论"
            visible={visible}
            onOk={clickAffirm}
            onCancel={hideModal}
            okText="确认"
            cancelText="取消"
          >
            <div className='modal_mask' style={{height:'150px'}}>
                <TextArea
                value={val}
                onChange={ChangeVal as any}
                placeholder="支持MarkDown"
                autoSize={{ minRows: 6, maxRows: 10 }}
              />
            </div>
          </Modal>
        </>
    )
}
export default PopUp