import React, { useState } from 'react'
import { Form, Input, Select, Row, Col, Button } from 'antd';
import { Values } from '@/types/comment'
interface Props {
    onFinish(values: Values): void
}
const Froms: React.FC<Props> = (props) => {
    const [form] = Form.useForm();
    const { onFinish } = props
    const onReset = () => {
        form.resetFields();
    };
    return (
        <Form style={{ marginLeft: '30px' }} form={form} name="control-hooks" layout="inline" onFinish={onFinish}>
            <Row gutter={24}>
                <Form.Item
                    name='name'
                    label="称呼"
                    rules={[{ message: 'Please input your username!' }]}
                >
                    <Input placeholder='请输入称呼' />
                </Form.Item>
                <Form.Item
                    name='email'
                    label="Email"
                >
                    <Input placeholder="请输入联系方式" />
                </Form.Item>
                <Form.Item label="Select" name="pass" style={{ width: '120px' }}>
                    <Select>
                        <Select.Option value={1}>已通过</Select.Option>
                        <Select.Option value={0}>未通过</Select.Option>
                    </Select>
                </Form.Item>
            </Row>

            <Col span={24} style={{ textAlign: 'right' }}>
                <Form.Item>
                    <Button type="primary" htmlType="submit" >
                        搜索
                    </Button>
                    <Button htmlType="button" onClick={onReset} style={{ margin: '0 8px' }}>
                        重置
                    </Button>
                </Form.Item>
            </Col>
        </Form>
    )
}
export default Froms