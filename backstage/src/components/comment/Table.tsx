import React,{ ReactNode, useState} from 'react'
import styles from '@/pages/comment/comment.less'
import { Table , Button , Popconfirm, message ,Tooltip, ConfigProvider} from 'antd';
import {Payload,ICommentItem} from '@/types/comment'
import {observer} from 'mobx-react-lite';
import useStore from '@/context/useStore';
import zhCN from 'antd/es/locale/zh_CN'; 

interface IProps{
    page:number,
    pageSize:string[],
    initData({page,pageSize,name,email,pass}:Payload):void,
    columns:any
    flag:boolean,
    commentList:ICommentItem,
    total:number,
    onShowSizeChange(current:number,size:number):void
}

const Tables:React.FC<IProps> =(props)=>{
    const store = useStore()
    const {initData,page,pageSize,columns,flag,commentList,total,onShowSizeChange} = props

   //设置Table表格load动画
//    const [flag,setFlag]=useState(false)
   const [arrId,setArrId]=useState([])
     //antd 复选框
     const [selectedRowKeys,setSelectedRowKeys]=useState([]);
     const onSelectChange = (selectedRowKeys:React.SetStateAction<never[]>) => {
         console.log('selectedRowKeys changed: ', selectedRowKeys);
         setArrId(selectedRowKeys)
         setSelectedRowKeys( selectedRowKeys);
        };
        //antd 的复选框
    const rowSelection = {
        selectedRowKeys,
        onChange:onSelectChange,
    };
        const hasSelected = selectedRowKeys.length > 0;
      //刷新
      const Refresh=()=>{
        initData({page})
      }
     //点击全选的通过
     const setPass=async ()=>{
        await arrId.forEach (id=>{
           store.comment.setAction({id,pass:true})
         })     
       setTimeout(() => {
        initData({page})
       }, 100);
        message.success('操作成功')
      }
       //点击全选的拒绝按钮
    const setRefuse=async ()=>{
      
        await arrId.forEach (async(id)=>{
           store.comment.setAction({id,pass:false})
         })     
         setTimeout(() => {
          initData({page})
         }, 100);
        message.success('操作成功')
      } 
       // 点击全选的删除提示框
    //删除成功的回调
   async function confirm() {
    await arrId.forEach (id=>{
      store.comment.setDelete(id)
    })     
    setTimeout(() => {
      initData({page})
     }, 100);
   message.success('操作成功')

   const itemRender =(page:number,type:string) => {
      
   }
  }
  
    return (
        <>
             <div className={styles.comment_table}>
              <div style={{ marginBottom: 16 }} className={styles.comment_table_button}>
                  <div className={styles.comment_table_button_icon}>
                  <Tooltip placement="top" title='刷新'>
                    <i onClick={Refresh} className={'iconfont icon-shuaxin'}></i>
                  </Tooltip>
                  </div>
                  <div className={styles.comment_table_box}>
                  {hasSelected ? 
                    <ul className={styles.comment_table_button}>
                         <li><Button onClick={setPass}>通过</Button></li>
                         <li><Button onClick={setRefuse}>拒绝</Button></li>
                         <li>
                         <Popconfirm
                          title="确认删除?"
                          onConfirm={confirm}
                          okText="确认"
                          cancelText="取消"
                        >
                          <Button danger>删除</Button>
                        </Popconfirm>   
                        </li>
                    </ul>
                  : ''}
                  </div>
              </div>
              <ConfigProvider locale={zhCN}>
              <Table  
                  rowKey="id"   
                  tableLayout={'fixed'}
                  loading={flag}
                  rowSelection={rowSelection as any} 
                  columns={columns as any} 
                  dataSource={commentList  as any} 
                  scroll={{ x: 1400}} 
                  pagination={{
                    showSizeChanger:true,
                    defaultPageSize:8,     
                    pageSizeOptions:pageSize,                       
                    showTotal:(total,range)=>{
                      return `共${total}条`
                    },
                    }}
                  style={{overflow:'hiddle'}} 
                />

              </ConfigProvider>
              
            </div>
        </>
    )
}
export default  observer(Tables)