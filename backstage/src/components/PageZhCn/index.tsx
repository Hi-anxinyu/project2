import zhCN from 'antd/es/locale/zh_CN';
import { ConfigProvider } from 'antd';

const PageZhCn: React.FC = (props) => { // 表格page改为中文
  return <ConfigProvider locale={zhCN}>
    {props.children}
  </ConfigProvider>
}

export default PageZhCn;