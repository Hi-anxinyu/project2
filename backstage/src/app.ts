import { RequestConfig, history } from 'umi';
import { message } from 'antd';
import StateContext from './context/stateContext';
import React from 'react';
import store from '@/models/index';
import { getToken } from './utils';
const baseUrl = 'https://creationapi.shbwyz.com';
//全部路由配置
const whiteList = ['/login', '/register'];
export function onRouteChange({ matchedRoutes, routes, location }: any) {
  let authorization = getToken();
  if (authorization !== 'Bearer undefined') {
    if (location.pathname === '/login') {
      history.replace('/');
    }
  } else {
    if (whiteList.indexOf(location.pathname) === -1) {
      history.replace(`/login?from=${encodeURIComponent(location.pathname)}`);
    }
  }
}
export function rootContainer(container: React.ReactNode) {
  return React.createElement(
    StateContext.Provider,
    { value: store },
    container,
  );
}
export const request: RequestConfig = {
  timeout: 100000,
  errorHandler(){
    return arguments[0].data
  },
  errorConfig: {
    adaptor: (resData) => {
      return {
        ...resData,
        success: resData.ok,
        errorMessage: resData.message,
      };
    },
  },
  middlewares: [],
  requestInterceptors: [
    (url, options) => {
      let authorization = getToken();
      if (authorization) {
        options = { ...options, headers: { authorization } };
      }
      return {
        url: `${baseUrl}${url}`,
        options,
      };
    },
  ],
  responseInterceptors: [
    (response) => {
      const codeMaps: { [key: number]: string } = {
        400: '错误的请求',
        403: '访客无权操作',
        404: '找不到资源',
        500: '服务器内部错误',
        502: '网关错误。',
        503: '服务不可用，服务器暂时过载或维护。',
        504: '网关超时。',
      };
      if (Object.keys(codeMaps).indexOf(String(response.status)) !== -1) {
        message.error(codeMaps[response.status]);
      }
      return response;
    },
  ],
};

// export const defineError =()=>{
//     console.log();

// }
