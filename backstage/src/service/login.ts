import { ILoginForm } from '@/types';
import {request} from 'umi';
//登录接口
export const getLoginData=({name,password}:ILoginForm)=>{
    return request('/api/auth/login',{
        method:"POST",
        data:{
            name,
            password,
        }
    })
}