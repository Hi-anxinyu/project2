import { request } from "@/.umi/plugin-request/request";


export function getArticleList({page = 1, pageSize = 12, title = ''}: {page?: number, pageSize?: number, title?: string, status?: string, category?: string}) { // 获取文章列表
  return request('/api/article', { 
    params: {
      page,
      pageSize,
      title,
    }
  })
}

export function getCategoryList() { // 获取分类列表
  return request('/api/category')
}

export function changeArticleStatus({id, status}: {id: string, status: string}) { // 改变文章状态
  return request(`/api/article/${id}`, {
    method : 'PATCH',
    data: {
      status,
    }
  })
}

export function changeArticleRecommended({id, isRecommended}: {id: string, isRecommended: boolean}) { // 改变首焦
  return request(`/api/article/${id}`, {
    method : 'PATCH',
    data: {
      isRecommended,
    }
  })
}

export function deleteRaticle(id: string) { // 改变首焦
  return request(`/api/article/${id}`, {
    method : 'DELETE',
  })
}