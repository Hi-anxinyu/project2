import {request} from 'umi'
import {IPayloadts} from '@/types'

export function getSettingDate(){
    return request('/api/setting/get',{method:'POST'})
}

export function setSave(data:IPayloadts){
    return request('/api/setting',{method:'POST',data})
}