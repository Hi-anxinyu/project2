import { ISearchList } from '@/types'
import { request } from 'umi'

// 获取搜索页面数据
export function getSearchList(page: number, pageSize: number, params: Partial<ISearchList> = {}) {
    return request(`/api/search?page=${page}&pageSize=${pageSize}`, { params })
}
//删除数据
export function deleteSearchList(id: string[]) {
    return request(`/api/search/${id}`, {
        method: 'DELETE'
    });
}