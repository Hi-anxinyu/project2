import {request} from 'umi';
export interface IPageParams{
    page:number,
    pageSize:number,
    name:string,
    path:string,
    status:string,
}
export const getPageList = (params:Partial<IPageParams>) => request('/api/page',{
    params,
})

//发布 下线 
export const publishPage = ({type,id}:{type:string,id:string}) => {
    if(type==='publish'){
        return request(`/api/page/${id}`,{
            method:"PATCH",
        })
    }else if(type==='offline'){
        return request(`/api/page/${id}`,{
            method:"PATCH",
        })
    }else{
        return request(`/api/page/${id}`,{
            method:"DELETE",
        })
    }   
}