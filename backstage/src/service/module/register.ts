import {request} from 'umi';

export const register = (data:{name:string,password:string,confirm:string})=> request('/api/user/register',{
    method:'POST',
    data,
})