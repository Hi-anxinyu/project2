import { request } from "@/.umi/plugin-request/request";
import { IArticleList } from "@/types";

export function getTagList() { // 获取标签列表
  return request('/api/tag', {
    method: 'GET',
  })
};

export function gitFileList({ page = 1, pageSize = 12 }: { page?: number, pageSize?: number }) { 
  return request('/api/file', {
    method: 'GET',
    params: {
      page,
      pageSize
    }
  })
};

export function getEditorDetail(id: string) { // 获取详情页文章列表
  return request(`/api/article/${id}`);
}

export function publishArticle(data: IArticleList) { // 发布详情文章
  return request(`/api/article/${data.id}`, {
    method: 'PATCH',
    data,
  })
};

export function newPublishArticle(data: IArticleList) { // 发布新建文章
  return request(`/api/article`, {
    method: 'POST',
    data
  })
}