import {request} from 'umi';

//更新用户信息
export const updateUserInfo = (params:{name:string,email:string}) => request('/api/user/update',{
    method:"POST",
    data:params,
})