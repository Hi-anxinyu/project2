import {request} from 'umi'
import {Payload,IAction, IPayloadTo} from '../../types/comment'
//获取数据
export function getCommentList({page,pageSize=12,name,email,pass}:Payload){
    return request(`/api/comment`,{params:{pageSize,page,name,email,pass}})     
}
//通过和拒绝
export function setAction(data:IAction){
    return request(`/api/comment/${data.id}`,{method:'PATCH',data}) 
}
//删除接口
export function setDelete(id:string){
    return request(`/api/comment/${id}`,{method: 'DELETE'})
}
//回复
export function setReply(data:IPayloadTo){
    return request(`/api/comment`,{method: 'POST',data})
}
