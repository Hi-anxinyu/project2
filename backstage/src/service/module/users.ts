import {request} from 'umi'
import {IPayload, IUserItem} from '@/types/index'
export function getUserList({name,page,pageSize=12,role,status,email}:IPayload){
    return request('/api/user',{params: {name,page,pageSize,role,status,email}})
}
export function getUserUpdate(data:IUserItem){
    return request('/api/user/update',{method: 'POST',data})
} 