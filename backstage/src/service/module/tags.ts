import { request } from "@/.umi/plugin-request/request";

export function addTagsList(data: {label: string, value: string}) { // 添加标签列表
  return request('/api/tag', {
    method: 'POST',
    data
  })
};

export function removeTagsList(id: string) { // 删除标签列表
  return request(`/api/tag/${id}`, {
    method: 'DELETE',
  })
};