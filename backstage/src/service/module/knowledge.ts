import { IAddKnowlwdge } from '@/types';
import { IFileSearch, IKnowledgeSearch } from '@/types/search';
import { request } from 'umi';

// 获取知识小册
export function getKnowledgeList(page: number, pageSize: number, params: Partial<IKnowledgeSearch> = {}) {
    return request(`/api/knowledge?page=${page}&pageSize=${pageSize}`, { params })
}

//删除
export function deleteKnowledgeList(id: string) {
    return request(`/api/knowledge/${id}`, {
        method: 'DELETE'
    })
}

//新增
//no
export function addKnowledgeList({ title, summary, isCommentable, cover }: Partial<IAddKnowlwdge> = {}) {
    return request(`/api/knowledge/book`, {
        method: 'POST',
        data: {
            title,
            summary,
            isCommentable,
            cover
        }
    })
}

//设为草稿 发布上线
export function changeKnowledgeList(id: string, status: string) {
    return request(`/api/knowledge/${id}`, {
        method: 'PATCH',
        data: {
            status
        }
    })
}

//编辑接口
export function getEditor(id: string) {
    return request(`/api/knowledge/${id}`)
}

//获取文件接口
export function getFileList(page: number, pageSize: number, params: Partial<IFileSearch> = {}) {
    return request(`/api/file?page=${page}&pageSize=${pageSize}`, { params });
}

//更新数据
export function setKnowledgeList(id: string, { title, summary, isCommentable, cover }: Partial<IAddKnowlwdge> = {}) {
    return request(`/api/knowledge/${id}`, {
        method: 'PATCH',
        data: {
            title,
            summary,
            isCommentable,
            cover
        }
    })
}