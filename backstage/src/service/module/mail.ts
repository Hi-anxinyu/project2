import {request} from 'umi'
import { ISmtpItem } from '@/types/mail'

//获取邮件列表
export function getSmtpList(params?:Partial<ISmtpItem>,page=1,pageSize=12){
    return request('/api/smtp',{params:{page,pageSize,...params}})
}

//删除邮件的接口
export function deleteMail(id:string){
    return request(`/api/smtp/${id}`,{
        method:'DELETE'
    })
}