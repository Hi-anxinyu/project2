import { request } from "@/.umi/plugin-request/request";

export function addCategoryList(data: {label: string, value: string}) { // 添加分类列表数据
  return request('/api/category', {
    method: 'POST',
    data,
  })
}

export function deleteCategoryList(id: string) {
  return request(`/api/category/${id}`, {
    method: 'DELETE',
  })
}

export function updateCategory(data: {label: string, value: string, id: string}) { // 添加分类列表数据
  return request(`/api/category${data.id}`, {
    method: 'PATCH',
    data,
  })
}