import React, { useState } from 'react';
import { Layout, Menu, Col, Row, Dropdown, Avatar, Button, Popover } from 'antd';
import { GithubOutlined } from '@ant-design/icons';
import SubMenu from 'antd/lib/menu/SubMenu';
import { NavLink, useHistory } from "react-router-dom"
import { INavItem } from '@/types';
import { useLocation, history } from 'umi';
import { MenuUnfoldOutlined, MenuFoldOutlined, UserOutlined } from '@ant-design/icons';
import './index.less'
import '@/utils/font/iconfont.css';
import '@/assets/css/global.less';
import useStore from '@/context/useStore'
import { MenuInfo } from 'rc-menu/lib/interface';
import {observer} from 'mobx-react-lite'
const whiteList = ['/login', '/register', '/article/editor', '/article/amEditor'];

const navdata = require('@/assets/data/nav');
const layout: React.FC = (props) => {
    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed] = useState(false);
    const [visible, setvisible] = useState(false);
    const location = useLocation();
    const history = useHistory()
    const store = useStore();
    const { logout, userInfo } = store.user;
    const toggle = () => {
        setCollapsed(!collapsed)
    };
    // console.log(location);
    const RegEditot = /editor/; // 正则匹配编辑页面;
    const handleMenuClick = (e: MenuInfo) => {
        switch (e.key) {
            //个人中心
            case '1': { history.replace('/ownspace');} break;
            //用户管理
            case '2': { history.replace('/user')} break;
            //系统设置
            case '3': { history.replace('/setting')} break;
            //退出登录
            case '4': {
                logout();
                localStorage.clear();
                history.replace(`/login?from=${encodeURIComponent(location.pathname)}`)
            } break;
            default: break;
        }
        setvisible(false);
    };
    const handleVisibleChange = (flag: boolean) => {
        setvisible(flag)
    };
    const menu = (
        <Menu onClick={handleMenuClick}>
            <Menu.Item key="1">个人中心</Menu.Item>
            <Menu.Item key="2">用户管理</Menu.Item>
            <Menu.Item key="3">系统设置</Menu.Item>
            <Menu.Item key="4">退出登录</Menu.Item>
        </Menu>
    );
    const menu1 = (
        <Menu onClick={handleMenuClick}>
            <Menu.Item key="5"><NavLink to='/article/amEditor'>新建文章-协同编辑器</NavLink></Menu.Item>
            <Menu.Item key="6"><NavLink to='/article/editor'>新建文章</NavLink></Menu.Item>
            <Menu.Item key="7"><NavLink to='/page/editor'>新建页面</NavLink></Menu.Item>
        </Menu>
    );
    if (whiteList.indexOf(location.pathname) !== -1 || RegEditot.test(location.pathname)) {
        return (    
            <>{props.children}</>
        )
    }
    return (
        <div className='container'>
            <Layout>
                <Sider trigger={null} collapsible collapsed={collapsed}>
                    <div className='rv'>
                        <img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-07-26/logo2.4a842ec4.png" alt="奥里给" />
                        <span style={{ marginLeft: 4 }}>{!collapsed&&'管理后台'}</span>
                    </div>
                    <div className="btn">
                        <Dropdown overlay={menu1} placement="bottomCenter">
                            <Button type='primary' size='large' style={{ width: "100%" }}>+{!collapsed&&'新建'}</Button>
                        </Dropdown>
                    </div>
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        {
                            navdata.map((item: INavItem, index: number) => {
                                return item.path ? <Menu.Item key={index} icon={<span className={item.icon} />}>
                                    <NavLink to={item.path}>{item.text}</NavLink>
                                </Menu.Item> : <SubMenu key="sub1"
                                    icon={<span className={item.icon} />}
                                    title={!collapsed && item.text}
                                >
                                    {
                                        item.children!.map((value, index) => {
                                            return <Menu.Item key={value.text} icon={<span className={value.icon} />}>
                                                <NavLink to={value.path}>{value.text}</NavLink>
                                            </Menu.Item>
                                        })
                                    }
                                </SubMenu>
                            })
                        }
                    </Menu>
                </Sider>
                <Layout className="site-layout site-wraper">
                    <Header className="site-layout-background a" style={{ padding: 0 }}>
                        <Row>
                            <Col span={12}>
                                {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                    className: 'trigger' ,
                                    onClick: toggle,
                                })}
                            </Col>
                            <Col span={12}>
                                <div className='top_right'>
                                    <a href='https://github.com/fantasticit/wipi' className='togithub'>
                                        <GithubOutlined />
                                    </a>
                                    <Dropdown
                                        overlay={menu}
                                        onVisibleChange={handleVisibleChange}
                                        visible={visible}
                                    >
                                        <a className="ant-dropdown-link" onClick={(e) => e.preventDefault()}>
                                            <Avatar src={userInfo.avatar || (localStorage.user ? JSON.parse(localStorage.user).avatar : '')}
                                                icon={<UserOutlined />} size='small'
                                            />
                                            <span style={{ color: '#333', marginLeft: 5 }}>Hi!,{userInfo.name || (localStorage.user ? JSON.parse(localStorage.user).name : '')}</span>
                                        </a>
                                    </Dropdown>
                                </div>

                            </Col>
                        </Row>
                    </Header>
                    <Content
                        className="site-layout-background1"
                        style={{
                            // minHeight: 280,
                        }}
                    >
                        {props.children}
                    </Content>
                </Layout>
            </Layout>
        </div>
    )
}

export default observer(layout)